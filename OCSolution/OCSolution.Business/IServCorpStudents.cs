﻿using OCSolution.Business.Models.DTO;
using OCSolution.Business.Models.StudentPage;
using OCSolution.Persistance.Enums;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace OCSolution.Business
{
    public interface IServCorpStudents
    {
        Task<double>                   GetStudentAttendanceAsync(int userId);
        Task<(int, int)>               GetStudentAbsentsAsync(int userId);
        Task<double>                   GetStudentEffectivityAsync(int userId);
        Task<int>                      GetStudentCompletedLessonsAsync(int userId);
        Task<string>                   GetStudentFullNameAsync(int userId);
        Task<(string, string)>         GetStudentsLvlsAsync(int userId);
        Task<DtoPaginatorCorpStudents> GetCompanyCorpStudentsPageAsync(List<int> groupsId, int page);

        Task<List<DtoLessonForTestResult>> GetLessonsForTestResultAsync(int userId);

        Task UpdateCorpStudentCurrentLevel(int userId, string lvl);
        Task AddStudentTestResultAsync(DtoStudentTestResult testResult);

        Task<StudentHeaderLine> GetCompanyStudentPageHeaderlineAsync(IdentityType identityType, string groupIds, int userId);
        Task<StudentHeaderLine> GetGroupStudentPageHeaderlineAsync(IdentityType identityType, int groupId, int userId);
        Task<StudentHeaderLine> GetTeacherStudentPageHeaderlineAsync(IdentityType identityType, int teacherId, int userId);

        Task<DtoDoughnutTripleView> GetStudentHomeworkStatsAsync(int userId, DtoDatePicker datePicker);
        Task<DtoBarChart>          GetStudentListeningBarChartAsync(int userId, DtoDatePicker datePicker);
        Task<DtoBarChart>          GetStudentSpeakingBarChartAsync(int userId, DtoDatePicker datePicker);
        Task<DtoBarChart>          GetStudentGrammarBarChartAsync(int userId, DtoDatePicker datePicker);
        Task<DtoBarChart>          GetStudentReadingBarChartAsync(int userId, DtoDatePicker datePicker);
    }
}
