﻿namespace OCSolution.Business.Utils
{
    public enum CardAreaState
    {
        COMPANY_ADDNEW,
        COMPANY_EDIT,
        COMPANY_STATS,
        COMPANY_GROUPS,
        COMPANY_STUDENTS,
          GROUP_LESSONS,
          GROUP_SCHEDULER,
          GROUP_STATS,
          GROUP_STUDENTS,
        STUDENT_STAT,
    }

    public enum StudentPageState
    {
        COMPANY_STUDENT_PAGE,
          GROUP_STUDENT_PAGE,
        TEACHER_STUDENT_PAGE
    }
}
