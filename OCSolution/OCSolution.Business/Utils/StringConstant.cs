﻿namespace OCSolution.Business.Utils
{
    public static class StringConstant
    {
        // depricated templates
        public static string PageTitle => "page_title";
        public static string PageDescription => "page_description";

        // depricated ui styles
        public static string UiPropsTheme => "ui_theme";

        // identity
        public const string LOGIN = "login";

        // session
        public const string SESSION_USER = "session_user";

        // urls
        public const string IDENTITY_LOGIN               = "/identity/login";
        public const string COMPANY_GROUPS_SCHEDULERLINK = "/dasboard/scheduler/company";
        public const string GROUPS_LINK                  = "/dasboard/group";

        // cash state
        public const string AUTH_STATE         = "id_authenticate";
        public const string COMPANY_STATE      = "company_headerline_state";
        public const string COMPANY_AREA_STATE = "company_area_state";
        public const string GROUP_STATE        = "group_headerline_state";
        public const string GROUP_AREA_STATE   = "group_area_state";
        public const string STUDENT_STATE      = "student_headerline_state";

        //temp data
        public const string COMPANY_CRUD_ALERT       = "company_alert_block";
        public const string CORPLESSON_UPDATE_ALERT  = "corplesson_update_alert";
        public const string STUDENT_DATEPICKER_ALERT = "student_datepicker";
        public const string STUDENT_TESTRESULT_ALERT = "stud_testres_added";
    }
}
