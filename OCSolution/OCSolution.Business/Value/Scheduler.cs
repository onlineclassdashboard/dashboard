﻿using OCSolution.Business.Models.DTO;
using OCSolution.Persistance;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OCSolution.Business.Value
{
    public class Scheduler
    {
        public string       Header { get; set; }
        public List<Cell[]> Rows   { get; set; }
    }

    public class Cell
    {
        public DateTime Date { get; private set; }

        public bool IsEmptyCell           { get; private set; }
        public bool ContainsNewStatus     { get; private set; }
        public bool ContainsSuccessStatus { get; private set; }
        public bool ContainsProblemStatus { get; private set; }

        public DtoCellLesson CellLesson { get; set; }


        private readonly IRepoLessons _repoLessons;
        private readonly IRepoCorpLessons _repoCorpLessons;
        private readonly IRepoLessonsTranlations _tranlations;

        public Cell()
        {
            IsEmptyCell = true;
        }

        public Cell(
            int groupId,
            DateTime dateTime,
            IRepoLessons repoLessons,
            IRepoCorpLessons repoCorpLessons,
            IRepoLessonsTranlations tranlations)
        {
            IsEmptyCell = false;
            Date        = dateTime;

            _repoLessons     = repoLessons;
            _repoCorpLessons = repoCorpLessons;
            _tranlations     = tranlations;

            _ = FillCellAsync(groupId);
        }

        private async Task FillCellAsync(int groupId)
        {
            var lesson = await _repoLessons.GetCellLessonAsync(groupId, Date);

            if (lesson == null)
            {
                CellLesson = new DtoCellLesson
                {
                    IsEmpty = true
                };

                return;
            }

            var tranlt = await _tranlations.GetLessonTranlationsAsync(lesson.Id);

            CellLesson = new DtoCellLesson
            {
                IsEmpty    = false,
                GroupId    = groupId,
                LessonId   = lesson.Id,
                TeacherId  = lesson.TeacherId,
                LessonName = tranlt.Title
            };

            var corpLesson = await _repoCorpLessons.GetCorpLessonAsync(lesson.Id);

            ContainsNewStatus     = lesson.Start > DateTime.Now;
            ContainsSuccessStatus = !ContainsNewStatus;
            ContainsProblemStatus = corpLesson != null && corpLesson.HasProblem;
        }
    }
}
