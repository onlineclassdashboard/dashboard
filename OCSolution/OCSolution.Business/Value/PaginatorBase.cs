﻿using System;
using System.Collections.Generic;

namespace OCSolution.Business.Value
{
    public class PaginatorBase
    {
        public int PageSize   { get; private set; }
        public int PageNumber { get; private set; }
        public int TotalPages { get; private set; }

        public PaginatorBase(int count, int pageNumber, int pageSize)
        {
            PageSize   = pageSize;
            PageNumber = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
        }

        public bool HasPreviousPage
        {
            get
            {
                return (PageNumber > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (PageNumber < TotalPages);
            }
        }

        public List<int> GetPagerRow(int rowCount)
            => new PaginatorRow(TotalPages, PageNumber, TotalPages).PageArr;
    }
}
