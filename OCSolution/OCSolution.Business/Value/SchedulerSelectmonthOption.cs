﻿namespace OCSolution.Business.Value
{
    public class SchedulerSelectmonthOption
    {
        public byte      Value { get; set; }
        public bool IsSelected { get; set; }
        public string    Month { get; set; }
    }

    public class SchedulerSelectyearOption
    {
        public ushort    Value { get; set; }
        public bool IsSelected { get; set; }
        public string     Year { get; set; }
    }
}
