﻿using OCSolution.Business.Value;

using System.Collections.Generic;

namespace OCSolution.Business
{
    public interface IServScheduler
    {
        List<SchedulerSelectmonthOption> SelectmonthOptions { get; }
        List<SchedulerSelectyearOption>   SelectyearOptions { get; }

        Scheduler GetCurrentScheduler(int groupId);
        Scheduler GetCustomScheduler(int groupId, int year, int month);
    }
}
