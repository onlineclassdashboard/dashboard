﻿using OCSolution.Business.Models.CompanyPage;
using OCSolution.Persistance.Enums;

namespace OCSolution.Business.Models.StudentPage
{
    public class StudentHeaderLine : ComponentSubHeaderLine
    {
        public string PrevStateLink { get; set; }

        public StudentHeaderLine(IdentityType type)
            : base(type) { }
    }
}
