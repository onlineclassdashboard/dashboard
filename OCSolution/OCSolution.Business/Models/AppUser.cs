﻿using OCSolution.Persistance.Enums;

namespace OCSolution.Business.Models
{
    public class AppUser
    {
        public int AppUserId { get; set; }
        public int DbUserId  { get; set; }
        public IdentityType Type { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Avatar { get; set; }
    }
}
