﻿using OCSolution.Persistance.Enums;

namespace OCSolution.Business.Models.CompanyPage
{
    public class CompanyHeaderLine : ComponentSubHeaderLine
    {
        public (string, int) GroupBtnData { get; set; }
        public (string, int) StudsBtnData { get; set; }

        public string DisabledSelect
            => _type != IdentityType.Admin ?
                "disabled" : null;

        public CompanyHeaderLine(IdentityType type)
            : base(type) { }
    }
}
