﻿using OCSolution.Persistance.Enums;

using System.Collections.Generic;

namespace OCSolution.Business.Models.CompanyPage
{
    public class ComponentSubHeaderLine
    {
        protected readonly IdentityType _type;

        public List<SelectItem> Selects { get; set; }

        public ComponentSubHeaderLine()
        {
            Selects = new List<SelectItem>();
        }

        public ComponentSubHeaderLine(IdentityType type) : this()
        {
            _type = type;
        }
    }

    public class SelectItem
    {
        public int        ItemId { get; set; }
        public int      TargetId { get; set; }
        public bool   IsSelected { get; set; }
        public string   ItemName { get; set; }
    }
}
