﻿using OCSolution.Business.Models.CompanyPage;
using OCSolution.Persistance.Enums;

namespace OCSolution.Business.Models.GroupPage
{
    public class GroupHeaderLine : ComponentSubHeaderLine
    {
        public string PrevStateLink { get; set; } = "/dasboard/company";
        public (string, int) StudsBtnData  { get; set; }

        public GroupHeaderLine(IdentityType type)
            : base(type) { }
    }
}
