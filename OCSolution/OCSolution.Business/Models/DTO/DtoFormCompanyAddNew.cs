﻿using System.Collections.Generic;

namespace OCSolution.Business.Models.DTO
{
    public class DtoFormCompanyAddNew
    {
        public string CompanyName  { get; set; }
        public string Daterange    { get; set; }
        public string Login        { get; set; }
        public string Password     { get; set; }
        public List<int> PinnedGroups { get; set; }
    }

    public class DtoFormCompanyUpdate : DtoFormCompanyAddNew
    {
        public int Id   { get; set; }
        public int HrId { get; set; }
    }
}
