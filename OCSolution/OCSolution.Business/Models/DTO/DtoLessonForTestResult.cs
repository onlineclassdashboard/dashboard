﻿namespace OCSolution.Business.Models.DTO
{
    public class DtoLessonForTestResult
    {
        public int LessonId  { get; set; }
        public int TeacherId { get; set; }
    }
}
