﻿namespace OCSolution.Business.Models.DTO
{
    public class DtoPinnedStudent
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public string FullName { get; set; }
        public string StartLvl { get; set; }
        public string Email    { get; set; }
    }

    public class DtoFormPinnedStudent
    {

        public int Id { get; set; }
        public int GroupId { get; set; }
        public string StartLvl { get; set; }
    }
}
