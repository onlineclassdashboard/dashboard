﻿using System;

namespace OCSolution.Business.Models.DTO
{
    public class DtoStudentTestResult : DtoLessonForTestResult
    {
        public bool IsCompleted => true;

        public int      UserId   { get; set; }
        public string   Result   { get; set; }
        public string   TestType { get; set; }
        public DateTime Date     { get; set; }
    }
}
