﻿namespace OCSolution.Business.Models.DTO
{
    public class DtoCompanyCard
    {
        public DtoDoughnutTripleView HomWorks { get; set; }
        public DtoDoughnutChartView  Effectivity { get; set; }
        public DtoDoughnutChartView  Attendance { get; set; }
        public DtoDoughnutChartView  Tests { get; set; }
    }

    public class DtoDoughnutChartView
    {
        public double FirstProp { get; set; }
        public double SeconProp { get; set; }
    }

    public class DtoDoughnutTripleView : DtoDoughnutChartView
    {
        public double ThirdProp { get; set; }
    }
}
