﻿using System;
using System.Globalization;

namespace OCSolution.Business.Models.DTO
{
    public class DtoDatePicker
    {
        private const double _min_period = 30;

        public DateTime StartTime { get; private set; }
        public DateTime EndTime   { get; private set; }

        public DtoDatePicker(string inputDate)
        {
            if (IsChartPeriod(inputDate))
            {
                StartTime = DateTime.ParseExact(inputDate.Split(" - ")[0], "dd/MM/yyyy", CultureInfo.InvariantCulture);
                EndTime   = DateTime.ParseExact(inputDate.Split(" - ")[1], "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            else StartTime = DateTime.ParseExact(inputDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }

        public int CurrentInterval => (EndTime - StartTime).Days;

        public string MinIntervalStr => string.Format("{0} - {1}",
            EndTime.AddDays(_min_period).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
            EndTime.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture));

        private bool IsChartPeriod(string data)
            => data.Split(" - ").Length == 2;
    }
}
