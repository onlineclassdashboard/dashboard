﻿namespace OCSolution.Business.Models.DTO
{
    public class DtoBarChart
    {
        public string[] Dates { get; set; }
        public int[]    Datas { get; set; }
    }
}
