﻿using System;
using System.Threading.Tasks;

namespace OCSolution.Business.Models.DTO
{
    public class DtoLesson
    {
        public int                  LessonId  { get; set; }
        public Task<(int, string)>  Teacher   { get; set; }
        public DateTime             StartDate { get; set; }
        public DateTime             EndDate   { get; set; }
        public string               IsActive  { get; set; }
    }

    public class DtoCellLesson
    {
        public int GroupId   { get; set; }
        public int LessonId  { get; set; }
        public int TeacherId { get; set; }

        public bool IsEmpty  { get; set; }

        public string LessonName { get; set; }
    }
}
