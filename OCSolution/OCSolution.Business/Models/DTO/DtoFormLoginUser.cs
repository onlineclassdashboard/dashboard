﻿namespace OCSolution.Business.Models.DTO
{
    public class DtoFormLoginUser
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
