﻿namespace OCSolution.Business.Models.DTO
{
    public class DtoCompanyGroup
    {
        public int    GroupNumber_id { get; set; }
        public string TeacherName    { get; set; }
        public string SchedulerLink  { get; set; }
        //public DateTime LastLesson { get; set; }
        public string GroupLink      { get; set; }
        public (string, string) TeacherCreads { get; set; }
    }
}
