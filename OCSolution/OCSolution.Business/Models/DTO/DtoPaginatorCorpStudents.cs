﻿using OCSolution.Business.Value;

using System.Collections.Generic;

namespace OCSolution.Business.Models.DTO
{
    public class DtoPaginatorCorpStudents : PaginatorBase
    {
        public int PageCount   { get; set; }
        public int RowCount    { get; set; }

        public List<DtoCompanyCorpStudent> List { get; set; }

        public DtoPaginatorCorpStudents(
            int count, int pageNumber, int pageSize)
            : base(count, pageNumber, pageSize)
        {
            List = new List<DtoCompanyCorpStudent>();
        }

        public DtoPaginatorCorpStudents(int count,
            int  pageNumber, int pageSize, int pageCount) :
            this(count, pageNumber, pageSize)
        {
            PageCount = pageCount;
            RowCount  = count;
        }
    }

    public class DtoPaginatorCompanyGroups : PaginatorBase
    {
        public int PageCount { get; set; }
        public int RowCount  { get; set; }

        public List<DtoCompanyGroup> List { get; set; }

        public DtoPaginatorCompanyGroups(
            int count, int pageNumber, int pageSize)
            : base(count, pageNumber, pageSize)
        {
            List = new List<DtoCompanyGroup>();
        }

        public DtoPaginatorCompanyGroups(int count,
            int pageNumber, int pageSize, int pageCount) :
            this(count, pageNumber, pageSize)
        {
            PageCount = pageCount;
            RowCount  = count;
        }
    }

    public class DtoPaginatorLessons : PaginatorBase
    {
        public int PageCount { get; set; }
        public int RowCount { get; set; }

        public List<DtoLesson> List { get; set; }

        public DtoPaginatorLessons(
            int count, int pageNumber, int pageSize)
            : base(count, pageNumber, pageSize)
        {
            List = new List<DtoLesson>();
        }

        public DtoPaginatorLessons(int count,
            int pageNumber, int pageSize, int pageCount) :
            this(count, pageNumber, pageSize)
        {
            PageCount = pageCount;
            RowCount  = count;
        }
    }
}
