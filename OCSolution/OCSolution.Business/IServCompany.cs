﻿using OCSolution.Business.Models.CompanyPage;
using OCSolution.Business.Models.DTO;
using OCSolution.Persistance.Enums;
using OCSolution.Persistance.Models;

using System.Threading.Tasks;

namespace OCSolution.Business
{
    public interface IServCompany
    {
        Task                        UpdateCompanyAsync(DtoFormCompanyUpdate updateForm);
        Task<int>                   CreateNewCompanyAsync(DtoFormCompanyAddNew addNewForm);
        Task<Company>               GetSelectedCompanyAsync(int? companyId);
        Task<Company>               GetHrCompanyAsync(int hrId);
        Task<(string, string)>      GetHrCreds(int hrId);
        Task<CompanyHeaderLine>     GetCompanyHeaderLineAsync(IdentityType identityType, int? selectId);

        Task<DtoDoughnutChartView>  GetAverageEfficiencyAsync(int companyId, DtoDatePicker datePicker);
        Task<DtoDoughnutChartView>  GetAttendanceAsync(int companyId, DtoDatePicker datePicker);
        Task<DtoDoughnutChartView>  GetTestResultsAsync(int companyId, DtoDatePicker datePicker);
        Task<DtoDoughnutTripleView> GetHomeWorksResultAsync(int companyId, DtoDatePicker datePicker);
    }
}
