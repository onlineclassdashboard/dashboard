﻿using OCSolution.Business.Models.DTO;
using OCSolution.Business.Models.GroupPage;
using OCSolution.Persistance.Enums;
using OCSolution.Persistance.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace OCSolution.Business
{
    public interface IServGroups
    {
        Task                             CheckAndAddNewCorpStudentAsync(int groupId, int userId, string lvl);
        
        Task<DtoPinnedStudent>           GetNewPinnedStudentAsync(int groupId, int userId);
        Task<DtoPaginatorCompanyGroups>  GetCompanyGroupsPageAsync(List<int> groupsIds, int page);
        Task<DtoPaginatorLessons>        GetGroupLessonsPaageAsync(int groupId, int page);
        List<(int, int)>                 GetNewCompanyPinnedStudents(List<int> groupsIds);
        Task<List<int>>                  GetAllGroupsIdAsync();

        Task<GroupHeaderLine>            GetTeacherHeaderLineAsync(IdentityType identityType, int teacherId, int groupId);
        Task<GroupHeaderLine>            GetHeaderLineAsync(IdentityType identityType, int compId, int groupId);
        Task<string>                     GetGroupLessonsTitleAsync(int lessonId);

        Task<CorpLesson>                 CheckAndGetCorpLessonAsync(int groupId, int lessonId, int teacherId);

        Task<DtoDoughnutChartView>       GetAverageEfficiencyAsync(int groupId, DtoDatePicker datePicker);
        Task<DtoDoughnutChartView>       GetAttendanceAsync(int groupId, DtoDatePicker datePicker);
        Task<DtoDoughnutChartView>       GetTestResultsAsync(int groupId, DtoDatePicker datePicker);
        Task<DtoDoughnutTripleView>      GetHomeWorksResultAsync(int groupId, DtoDatePicker datePicker);

    }
}
