﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OCSolution.Business.Extensions
{
    public static class StringExtensions
    {
        public static int ToGroupCount(this string groups)
            => GetDataFrom(groups).Length;

        public static List<int> ToIdList(this string groups)
            => GetDataFrom(groups).Select(d => int
                .Parse(d)).ToList();

        public static int ToFirstId(this string groups)
            => groups.ToIdList().First();

        private static string[] GetDataFrom(string groups)
            => groups.Split(", ");

        public static string ToIdsString(this List<int> ids)
        {
            var sb = new StringBuilder(ids[0].ToString());

            for (int i = 1; i < ids.Count; i++)
            {
                sb.AppendFormat(", {0}", ids[i]);
            }

            return sb.ToString();
        }
    }
}
