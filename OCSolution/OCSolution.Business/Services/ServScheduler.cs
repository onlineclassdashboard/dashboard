﻿using Microsoft.Extensions.Options;

using OCSolution.Business.Options;
using OCSolution.Business.Value;
using OCSolution.Persistance;

using System;
using System.Collections.Generic;
using System.Linq;

namespace OCSolution.Business.Services
{
    public class ServScheduler : IServScheduler
    {
        private readonly IRepoLessons            _repoLessons;
        private readonly IRepoCorpLessons        _repoCorpLessons;
        private readonly IRepoLessonsTranlations _tranlations;

        private readonly UiOptions _uiOptions;

        private const int  _days_count = 7;
        private const int _month_count = 12;
        private const int  _year_count = 30;

        private int _year;
        private int _month;
        private int _groupId;

        private DateTime _firstDay;

        public ServScheduler(
            IRepoLessons            repoLessons,
            IRepoCorpLessons        repoCorpLessons,
            IRepoLessonsTranlations tranlations,
            IOptions<UiOptions>     options)
        {
            _repoLessons     = repoLessons;
            _repoCorpLessons = repoCorpLessons;
            _tranlations     = tranlations;
            _uiOptions       = options.Value;
        }

        public List<SchedulerSelectmonthOption> SelectmonthOptions
            => GetSelectMontOptions();
        private List<SchedulerSelectmonthOption> GetSelectMontOptions()
        {
            var options = new List<SchedulerSelectmonthOption>();

            for (int i = 1; i <= _month_count; i++)
            {
                options.Add(new SchedulerSelectmonthOption
                {
                    Value = (byte)i,
                    IsSelected = DateTime.Now.Month == i,
                    Month = i switch
                    {
                        1 => _uiOptions.Month[0],
                        2 => _uiOptions.Month[1],
                        3 => _uiOptions.Month[2],
                        4 => _uiOptions.Month[3],
                        5 => _uiOptions.Month[4],
                        6 => _uiOptions.Month[5],
                        7 => _uiOptions.Month[6],
                        8 => _uiOptions.Month[7],
                        9 => _uiOptions.Month[8],
                        10 => _uiOptions.Month[9],
                        11 => _uiOptions.Month[10],
                        12 => _uiOptions.Month[11],
                        _ => throw new Exception("unknown month")
                    }
                });
            }

            return options;
        }

        public List<SchedulerSelectyearOption> SelectyearOptions
            => GetSelectyearOptions();
        private List<SchedulerSelectyearOption> GetSelectyearOptions()
        {
            var options = new List<SchedulerSelectyearOption>();

            for (int i = 2009; i <= (2009 + _year_count); i++)
            {
                options.Add(new SchedulerSelectyearOption
                {
                    Value = (ushort)i,
                    IsSelected = DateTime.Now.Year == i,
                    Year = i.ToString()
                });
            }

            return options;
        }

        public Scheduler GetCurrentScheduler(int groupId)
        {
            _groupId  = groupId;
            _year     = DateTime.Now.Year;
            _month    = DateTime.Now.Month;
            _firstDay = new DateTime(_year, _month, 1);

            var scheduler = new Scheduler
            {
                Header = GetHeader(),
                Rows   = GetRows()
            };

            return scheduler;
        }

        public Scheduler GetCustomScheduler(int groupId, int year, int month)
        {
            _groupId  = groupId;
            _year     = year;
            _month    = month;
            _firstDay = new DateTime(_year, _month, 1);

            var scheduler = new Scheduler
            {
                Header = GetHeader(),
                Rows   = GetRows()
            };

            return scheduler;
        }

        private List<Cell[]> GetRows()
        {
            var rows = new List<Cell[]>
            {
                GetFirstRow()
            };

            bool endFlag;
            do
            {
                var next = NextRow(rows.Last().Last().Date, out bool isEnd);
                endFlag  = isEnd;

                rows.Add(next);
            }
            while (!endFlag);

            return rows;
        }

        private Cell[] GetFirstRow()
            => _firstDay.DayOfWeek switch
            {
                DayOfWeek.Tuesday   => FirstRow(1),
                DayOfWeek.Wednesday => FirstRow(2),
                DayOfWeek.Thursday  => FirstRow(3),
                DayOfWeek.Friday    => FirstRow(4),
                DayOfWeek.Saturday  => FirstRow(5),
                DayOfWeek.Sunday    => FirstRow(6),
                                  _ => NextRow(_firstDay.AddDays(-1), out _)
            };
        private Cell[] FirstRow(int emptyCount)
        {
            int fullDaysCount = _days_count - emptyCount;

            var firstRow = EmptyRangeCells(emptyCount);

            firstRow.AddRange(AfterEmptyCells(fullDaysCount));

            return firstRow.ToArray();
        }

        private List<Cell> EmptyRangeCells(int count)
        {
            var cells = new List<Cell>();

            for (int i = 0; i < count; i++)
            {
                cells.Add(new Cell());
            }

            return cells;
        }
        private List<Cell> AfterEmptyCells(int count)
        {
            var cells = new List<Cell>
            {
                new Cell(_groupId, _firstDay, _repoLessons, _repoCorpLessons, _tranlations)
            };

            if (count == 1) return cells;

            for (int i = 1; i < count; i++)
            {
                cells.Add(new Cell(_groupId, _firstDay.AddDays(i), _repoLessons, _repoCorpLessons, _tranlations));
            }

            return cells;
        }

        private Cell[] NextRow(DateTime prevEnd, out bool isEnd)
        {
            isEnd = false;

            int days = _days_count;
            var cells = new List<Cell>();

            for (int i = 1; i <= _days_count; i++)
            {
                days -= 1;

                var date = prevEnd.AddDays(i);

                if (date.Month == _month) cells.Add(new Cell(_groupId, date, _repoLessons, _repoCorpLessons, _tranlations));
                else
                {
                    isEnd = true;
                    break;
                }
            }

            if (isEnd)
            {
                cells.AddRange(EmptyRangeCells(days + 1));
                return cells.ToArray();
            }

            return cells.ToArray();
        }

        private string GetHeader()
            => _month switch
            {
                 1 => $"{_uiOptions.Month[0]}  {_year}",
                 2 => $"{_uiOptions.Month[1]}  {_year}",
                 3 => $"{_uiOptions.Month[2]}  {_year}",
                 4 => $"{_uiOptions.Month[3]}  {_year}",
                 5 => $"{_uiOptions.Month[4]}  {_year}",
                 6 => $"{_uiOptions.Month[5]}  {_year}",
                 7 => $"{_uiOptions.Month[6]}  {_year}",
                 8 => $"{_uiOptions.Month[7]}  {_year}",
                 9 => $"{_uiOptions.Month[8]}  {_year}",
                10 => $"{_uiOptions.Month[9]}  {_year}",
                11 => $"{_uiOptions.Month[10]} {_year}",
                12 => $"{_uiOptions.Month[11]} {_year}",
                 _ => throw new NotImplementedException("Not month format")
            };
    }
}
