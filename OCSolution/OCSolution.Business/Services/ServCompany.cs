﻿using Microsoft.Extensions.Options;

using OCSolution.Business.Extensions;
using OCSolution.Business.Models.CompanyPage;
using OCSolution.Business.Models.DTO;
using OCSolution.Business.Options;
using OCSolution.Persistance;
using OCSolution.Persistance.Enums;
using OCSolution.Persistance.Models;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using static OCSolution.Persistance.Enums.HomeWorkType;

namespace OCSolution.Business.Services
{
    public class ServCompany : ServBase, IServCompany
    {
        private readonly IRepoUsers            _repoUsers;
        private readonly IRepoIdenties         _repoIdenties;
        private readonly IRepoCompanies        _repoCompanies;
        private readonly IRepoGroups           _repoGroups;
        private readonly IRepoGroupStudents    _repoGroupStudents;
        private readonly IRepoLessons          _repoLessons;
        private readonly IRepoTestResults      _repoTestResults;
        private readonly IRepoCorpStudentStats _repoCorpStudentStats;
        private readonly IServScheduler        _servScheduler;

        private readonly AppOptions _appOptions;

        private const byte   _passSize  = 7;
        private const string _passChars = "abcdefghijkmnpqrstuvwxyz123456789";

        public ServCompany(
            IRepoUsers         repoUsers,
            IRepoIdenties      repoIdenties,
            IRepoCompanies     repoCompanies,
            IRepoGroups        repoGroups,
            IRepoGroupStudents repoGroupStudents,
            IRepoLessons       repoLessons,
            IRepoTestResults   repoTestResults,
            IRepoCorpStudentStats repoCorpStudentStats,
            IServScheduler        servScheduler,
            IOptions<AppOptions>  appOpt)
        {
            _repoUsers         = repoUsers;
            _repoIdenties      = repoIdenties;
            _repoCompanies     = repoCompanies;
            _repoGroups        = repoGroups;
            _repoGroupStudents = repoGroupStudents;
            _repoLessons       = repoLessons;
            _repoTestResults   = repoTestResults;
            _repoCorpStudentStats = repoCorpStudentStats;
            _servScheduler        = servScheduler;
            _appOptions           = appOpt.Value;
        }

        #region Company HeaderLine Region
        public Task<Company> GetHrCompanyAsync(int hrId)
            => _repoCompanies.GetCompanyByHrIdAsync(hrId);

        public async Task<(string, string)> GetHrCreds(int hrId)
        {
            var identity = await _repoIdenties.GetHrIdentityAsync(hrId);

            if (identity == null) return (string.Empty, string.Empty);

            return (identity.Login, identity.Password);
        }

        public Task<Company> GetSelectedCompanyAsync(int? companyId)
        {
            if (companyId == null || companyId == 0) return _repoCompanies.GetFirstCompanyAsync();
            else return _repoCompanies.GetCompanyByIdAsync((int)companyId);
        }

        public async Task<CompanyHeaderLine> GetCompanyHeaderLineAsync(IdentityType identityType, int? selectId)
        {
            var headerline = new CompanyHeaderLine(identityType);

            var companies = await _repoCompanies.ToListAsync();

            foreach (var company in companies.OrderBy(c => c.Name))
            {
                headerline.Selects.Add(new SelectItem
                {
                    ItemId     = company.Id,
                    ItemName   = company.Name,
                    IsSelected = company.Id == selectId
                });

                if (company.Id == selectId)
                {
                    headerline.GroupBtnData =       GetGroupBtnDataFrom(company.PinnedGroups);
                    headerline.StudsBtnData = await GetStudentsBtnDataAsyncFrom(company.PinnedGroups);
                }
            }

            return headerline;
        }
        private (string, int) GetGroupBtnDataFrom(string pinnedGroups)
        {
            if (string.IsNullOrWhiteSpace(pinnedGroups)) return ("0", 0);
            else return (pinnedGroups, pinnedGroups.ToGroupCount());
        }
        private async Task<(string, int)> GetStudentsBtnDataAsyncFrom(string pinnedGroups)
        {
            if (string.IsNullOrWhiteSpace(pinnedGroups)) return ("0", 0);
            else return (pinnedGroups, await GetStudentsCountAsync(pinnedGroups.ToIdList()));
        }
        private Task<int> GetStudentsCountAsync(List<int> groups)
        {
            var tcs = new TaskCompletionSource<int>();

            var groupStudents = _repoGroupStudents.GetGroupsStudents(groups);

            tcs.SetResult(groupStudents.Count());

            return tcs.Task;
        }

        #endregion

        #region Company Card Region
        public async Task<DtoDoughnutChartView> GetAttendanceAsync(int companyId, DtoDatePicker datePicker)
        {
            if (companyId == 0) return null;

            var company = await _repoCompanies.GetCompanyByIdAsync(companyId);

            if (company                               == null ||
                company.PinnedGroups.ToIdList()       == null ||
                company.PinnedGroups.ToIdList().Count == 0) return null;

            var grLessons = await _repoLessons.GetGroupLessonsByIntervalAsync(
                company.PinnedGroups.ToIdList(), datePicker.StartTime, datePicker.EndTime);

            return new DtoDoughnutChartView
            {
                FirstProp = GetAttended(grLessons, out double apsent),
                SeconProp = apsent
            };
        }
        private double GetAttended(List<int> lessonIds, out double apsent)
        {
            int attendCount = 0;
            int appsent = 0;

            if (lessonIds.Count == 0)
            {
                apsent = appsent;
                return attendCount;
            }

            var corpStats = _repoCorpStudentStats.GetCorpstudentStatsAsync(lessonIds).Result;

            foreach (var stat in corpStats)
            {
                if (stat.Attendance) attendCount++;
                else appsent++;
            }

            apsent = corpStats.Count != 0 ? (double)appsent / corpStats.Count * 100 : 0;

            return corpStats.Count != 0 ? (double)attendCount / corpStats.Count * 100 : 0;
        }

        public async Task<DtoDoughnutChartView> GetTestResultsAsync(int companyId, DtoDatePicker datePicker)
        {
            if (companyId == 0) return null;

            var company = await _repoCompanies.GetCompanyByIdAsync(companyId);

            if (company                               == null ||
                company.PinnedGroups.ToIdList()       == null ||
                company.PinnedGroups.ToIdList().Count == 0) return null;

            var grStudUsers  = await _repoGroups.GetGroupStudentsUserAsync(company.PinnedGroups.ToIdList());
            var testResults  = await _repoTestResults.GetCorpTestResultsByIntervalAsync(grStudUsers, datePicker.StartTime, datePicker.EndTime);

            return new DtoDoughnutChartView
            {
                FirstProp = GetResults(testResults, out double secProp),
                SeconProp = secProp
            };
        }
        private double GetResults(List<CorpTestResult> testResults, out double secProp)
        {
            secProp = testResults.Count != 0 ? (double)testResults.Where(t => !t.Completed).Count() / testResults.Count * 100 : 0;

            return testResults.Count != 0 ? (double)testResults.Where(t => t.Completed).Count() / testResults.Count * 100 : 0;
        }

        public async Task<DtoDoughnutTripleView> GetHomeWorksResultAsync(int companyId, DtoDatePicker datePicker)
        {
            if (companyId == 0) return null;

            var company = await _repoCompanies.GetCompanyByIdAsync(companyId);

            if (company                         == null ||
                company.PinnedGroups.ToIdList() == null ||
                company.PinnedGroups.ToIdList().Count == 0) return null;

            var grLessons = await _repoLessons.GetGroupLessonsByIntervalAsync(
                company.PinnedGroups.ToIdList(), datePicker.StartTime, datePicker.EndTime);

            return new DtoDoughnutTripleView
            {
                FirstProp = GetHomeWorks(grLessons, out double no, out double yes),
                SeconProp = no,
                ThirdProp = yes
            };
        }
        private double GetHomeWorks(List<int> lessonIds, out double no, out double yes)
        {
            int done     = 0;
            int notDone  = 0;
            int partDone = 0;

            if (lessonIds.Count == 0)
            {
                no = yes = done;
            }

            var corpStats = _repoCorpStudentStats.GetCorpstudentStatsAsync(lessonIds).Result;

            foreach (var stat in corpStats)
            {
                if (stat.HomeWork == Yes.ToString("G")) done++;
                if (stat.HomeWork == No.ToString("G")) notDone++;
                if (stat.HomeWork == Partial.ToString("G")) partDone++;
            }

            no  = corpStats.Count != 0 ? (double)notDone / corpStats.Count * 100 : Double.NaN;
            yes = corpStats.Count != 0 ? (double)done / corpStats.Count * 100 : Double.NaN;

            return corpStats.Count != 0 ? (double)partDone / corpStats.Count * 100 : Double.NaN;
        }

        public async Task<DtoDoughnutChartView> GetAverageEfficiencyAsync(int companyId, DtoDatePicker datePicker)
        {
            if (companyId == 0) return null;

            var company = await _repoCompanies.GetCompanyByIdAsync(companyId);

            if (company == null ||
                company.PinnedGroups.ToIdList() == null ||
                company.PinnedGroups.ToIdList().Count == 0) return null;

            var grLessons = await _repoLessons.GetGroupLessonsByIntervalAsync(
                company.PinnedGroups.ToIdList(), datePicker.StartTime, datePicker.EndTime);

            var grStudUsers = await _repoGroups.GetGroupStudentsUserAsync(company.PinnedGroups.ToIdList());
            var testResults = await _repoTestResults.GetCorpTestResultsByIntervalAsync(grStudUsers, datePicker.StartTime, datePicker.EndTime);

            var efficiency = GetEfficiency(grLessons, testResults);

            return new DtoDoughnutChartView
            {
                FirstProp = efficiency.Item1,
                SeconProp = efficiency.Item2
            };
        }
        private (double, double) GetEfficiency(List<int> lessonIds, List<CorpTestResult> testResults)
        {
            double attendance = GetAttended(lessonIds,  out double apsent);
            double homeWorks  = GetHomeWorks(lessonIds, out double no, out double yes);

            var testCompleted = GetEfficiencyTestResults(testResults, out double testUncompleted);
            double greenEff = CalculateEfficiency
            (
                attend: attendance,
                tests: testCompleted,
                hmWork: homeWorks + yes,
                _appOptions.AttendanceKoef,
                _appOptions.TestsKoef,
                _appOptions.HomeworkKoef
            );
            
           double redEff = CalculateEfficiency
           (
               attend: apsent,
               tests: testUncompleted,
               hmWork: no,
               _appOptions.AttendanceKoef,
               _appOptions.TestsKoef,
               _appOptions.HomeworkKoef
               );

           return NormalizePercents(greenEff, redEff);
        }

        public async Task UpdateCompanyAsync(DtoFormCompanyUpdate updateForm)
        {
            int id = await UpdateHrAsync(updateForm.HrId, updateForm.Login, updateForm.Password);
                     await UpdateSomeCompanyAsync(updateForm.Id, updateForm.CompanyName, updateForm.Daterange, updateForm.PinnedGroups, id);
        }
        private async Task<int> UpdateHrAsync(int hrId, string login, string password)
        {
            var hr = await _repoIdenties.GetHrIdentityAsync(hrId);

            if (hr == null) return await AddNewHRAsync(login, password);

            hr.Login    = login;
            hr.Password = password;

            await _repoIdenties.UpdateIdentityAsync(hr);

            return hr.Id;
        }
        private async Task UpdateSomeCompanyAsync(int id, string name, string daterange, List<int> groups, int hrId)
        {
            var company = await _repoCompanies.GetCompanyByIdAsync(id);

            company.Name          = name;
            company.PinnedGroups  = groups?.ToIdsString();
            company.LearningStart = DateTime.ParseExact(daterange, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            company.UpdatedAt     = DateTime.Now;
            company.HrId          = hrId;

            await _repoCompanies.UpdateCompanyAsync(company);
        }

        public Task<int> CreateNewCompanyAsync(DtoFormCompanyAddNew addNewForm)
        {
            _ = CheckAndCreateNewTeacher(addNewForm.PinnedGroups.ToIdsString());
            return AddNewCompanyAsync(addNewForm);
        }
        private async Task CheckAndCreateNewTeacher(string pinnedGroups)
        {
            var teachers = await _repoGroups
                .GetCompanyTeachersAsync(
                pinnedGroups.ToIdList());

            foreach (int id in teachers)
            {
                var teachIdent = await _repoIdenties.GetIdentityByUserIdAsync(id);

                if (teachIdent == null)
                {
                    var name = await _repoUsers.GetFullNameAsync(id);

                    await _repoIdenties.AddIdentityAsync(new CorpIdentity
                    {
                        UserId     = id,
                        Login      = "teacher",
                        FirstName  = name.Item2,
                        SecondName = name.Item1,
                        Type       = IdentityType.Teacher.ToString("G"),
                        Password   = GenerateNewPassword(_passSize, _passChars.ToArray()),
                        CreatedAt  = DateTime.Now
                    }); ;
                }
            } 
        }
        private async Task<int> AddNewCompanyAsync(DtoFormCompanyAddNew addNewForm)
        {
            var newCompany = new Company
            {
                HrId = await AddNewHRAsync(addNewForm.Login, addNewForm.Password),
                LearningStart = new DtoDatePicker(addNewForm.Daterange).StartTime,
                PinnedGroups  = addNewForm.PinnedGroups.ToIdsString(),
                Name          = addNewForm.CompanyName,
                CreatedAt     = DateTime.Now
            };

            await _repoCompanies.AddCompanyAsync(newCompany);

            return newCompany.Id;
        }
        private async Task<int> AddNewHRAsync(string login, string password)
        {
            await _repoIdenties.AddIdentityAsync(new CorpIdentity
            {
                Login = login,
                Type = IdentityType.HR.ToString("G"),
                Password = password,
                CreatedAt = DateTime.Now
            });

            var hr = await _repoIdenties.GetIdentityByLoginPasswordAsync(login, password);

            return hr.Id;
        }
        #endregion

        #region Scheduler Region

        #endregion

        private string GenerateNewPassword(byte size, char[] chars)
        {
            byte[] data = new byte[4 * size];

            using var crypto = new
                RNGCryptoServiceProvider();
            crypto.GetBytes(data);

            var sb = new StringBuilder(size);

            for (int i = 0; i < size; i++)
            {
                uint rnd = BitConverter.ToUInt32(data, i * 4);
                long idx = rnd % chars.Length;

                sb.Append(chars[idx]);
            }

            return $"{sb}";
        }
    }
}
