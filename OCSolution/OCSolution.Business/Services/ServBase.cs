﻿using System.Collections.Generic;
using System.Linq;
using OCSolution.Persistance.Models;
using static System.Double;

namespace OCSolution.Business.Services
{
    public class ServBase
    {
        
        protected double GetEfficiencyTestResults(List<CorpTestResult> testResults, out double secProp)
        {
            if (testResults.Count == 0)
            {
                secProp = NaN;
                return NaN;
            }

            int count = 0;

            foreach (var test in testResults)
            {
                if (test.Completed && int.TryParse(test.Result, out int result)) count += result;
            }

            secProp = (double)testResults.Where(t => !t.Completed).Count() / testResults.Count * 100;
            
    
            double completedPersent = (double) count / testResults.Count();
            if (completedPersent != 0 )
            {
                secProp = 100 - completedPersent;
            }

            return completedPersent;
        }
        
        public double CalculateEfficiency(double attend, double tests, double hmWork, double attendanceKoef, double testsKoef, double homeworkKoef)
        {
            var result = 0.0;
            var coeffCount = 0;
            if (attend != 0 && !IsNaN(attend))
            {
                result += attendanceKoef * attend;
                coeffCount++;
            }
            if (hmWork != 0 && !IsNaN(hmWork))
            {
                result += homeworkKoef * hmWork;
                coeffCount++;
            }
            if (tests != 0 && !IsNaN(tests))
            {
                result += testsKoef * tests;
                coeffCount++;
            }

            return coeffCount != 0 ? result / coeffCount : NaN;
        }

        protected (double, double) NormalizePercents(double greenEff, double redEff)
        {
            if (redEff == 0 && greenEff > 0)
            {
                greenEff = 100;
            } else if (redEff + greenEff != 100.0)
            {
                greenEff = greenEff * 100 / (greenEff + redEff);
                redEff = 100 - greenEff;
            }
            return (greenEff, redEff);
        }
    }
}