﻿using System;
using Microsoft.Extensions.Options;

using OCSolution.Business.Extensions;
using OCSolution.Business.Models.CompanyPage;
using OCSolution.Business.Models.DTO;
using OCSolution.Business.Models.GroupPage;
using OCSolution.Business.Options;
using OCSolution.Persistance;
using OCSolution.Persistance.Enums;
using OCSolution.Persistance.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static System.Double;

using static OCSolution.Business.Utils.StringConstant;
using static OCSolution.Persistance.Enums.HomeWorkType;

namespace OCSolution.Business.Services
{
    public class ServGroups : ServBase, IServGroups
    {
        private readonly IRepoIdenties           _repoIdenties;
        private readonly IRepoUsers              _repoUsers;
        private readonly IRepoGroups             _repoGroups;
        private readonly IRepoLessons            _repoLessons;
        private readonly IRepoCorpLessons        _repoCorpLessons;
        private readonly IRepoCompanies          _repoCompany;
        private readonly IRepoGroupStudents      _repoGroupStudents;
        private readonly IRepoGroupTranlations   _repoGroupTranlations;
        private readonly IRepoLessonsTranlations _repoLessonsTranlations;
        private readonly IRepoCorpStudents       _repoStudents;
        private readonly IRepoTestResults        _repoTestResults;
        private readonly IRepoCorpStudentStats   _repoCorpStudentStats;

        private readonly AppOptions _appOptions;

        private const byte _pageSize = 10;
        private const byte _pageSizeLessons = 16;

        public ServGroups(
            IRepoIdenties           repoIdenties,
            IRepoUsers              repoUsers,
            IRepoGroups             repoGroups,
            IRepoLessons            repoLessons,
            IRepoCorpLessons        repoCorpLessons,
            IRepoCompanies          repoCompany,
            IRepoGroupStudents      repoGroupStudents,
            IRepoGroupTranlations   repoGroupTranlations,
            IRepoLessonsTranlations repoLessonsTranlations,
            IRepoCorpStudents       repoStudents,
            IRepoTestResults        repoTestResults,
            IRepoCorpStudentStats   repoCorpStudentStats,
            IOptions<AppOptions>    appOpt)
        {
            _repoIdenties           = repoIdenties;
            _repoUsers              = repoUsers;
            _repoGroups             = repoGroups;
            _repoLessons            = repoLessons;
            _repoCorpLessons        = repoCorpLessons;
            _repoCompany            = repoCompany;
            _repoGroupStudents      = repoGroupStudents;
            _repoGroupTranlations   = repoGroupTranlations;
            _repoLessonsTranlations = repoLessonsTranlations;
            _repoStudents           = repoStudents;
            _repoTestResults        = repoTestResults;
            _repoCorpStudentStats   = repoCorpStudentStats;
            _appOptions             = appOpt.Value;
        }

        public Task<List<int>> GetAllGroupsIdAsync()
            => _repoGroups.GetAllGroupsIdsAsync();

        public List<(int, int)> GetNewCompanyPinnedStudents(List<int> groupsIds)
        {
            var grStuds  = _repoGroupStudents.GetGroupsStudents(groupsIds);

            if (grStuds == null || grStuds.Count == 0) return new List<(int, int)>();

            return grStuds.Select(g => (g.GroupId, g.UserId)).ToList();
        }

        public async Task CheckAndAddNewCorpStudentAsync(int groupId, int userId, string lvl)
        {
            var corpStudent = await _repoStudents.GetCorpStudentByIdAsync(userId);

            if (corpStudent == null)
            {
                await _repoStudents.AddNewCorpStudentAsync(new CorpStudent
                {
                    GroupId    = groupId,
                    StartLevel = lvl,
                    Id         = userId
                });

                return;
            }

            corpStudent.StartLevel = lvl;

            await _repoStudents.UpdateCorpStudentAsync(corpStudent);
        }

        public async Task<DtoPinnedStudent> GetNewPinnedStudentAsync(int groupId, int userId)
        {
            var user     = await _repoUsers.GetUserByIdAsync(userId);
            var corpStud = await _repoStudents.GetCorpStudentByIdAsync(userId);

            if (corpStud == null) return new DtoPinnedStudent
            {
                Id       = userId,
                GroupId  = groupId,
                FullName = $"{user?.SName} {user?.FName}",
                Email    = user?.Email
            };

            return new DtoPinnedStudent
            {
                Id       = corpStud.Id,
                GroupId  = corpStud.GroupId,
                FullName = $"{user?.SName} {user?.FName}",
                StartLvl = corpStud.StartLevel,
                Email    = user?.Email
            };
        }

        public async Task<DtoPaginatorCompanyGroups> GetCompanyGroupsPageAsync(List<int> groupsIds, int page)
        {
            var dbGroups = await _repoGroups.GetPagedGroupsResult(groupsIds, page, _pageSize);
            if (dbGroups == null || dbGroups.Results.Count == 0) return null;

            var paginator = new DtoPaginatorCompanyGroups(
                dbGroups.RowCount, page, _pageSize, dbGroups.PageCount);

            foreach (var group in dbGroups.Results)
            {
                paginator.List.Add(new DtoCompanyGroup
                {
                    GroupNumber_id = group.Id,
                    TeacherName    = await GetTeacherNameAsync(group.TeacherId),
                    TeacherCreads  = await GetTeacherCreds(group.TeacherId),
                    SchedulerLink  = COMPANY_GROUPS_SCHEDULERLINK,
                    GroupLink      = GROUPS_LINK
                });
            }

            return paginator;
        }
        private async Task<string> GetTeacherNameAsync(int teacherId)
        {
            var user = await _repoUsers.GetUserByIdAsync(teacherId);

            return $"{user.FName} {user.SName}";
        }
        private async Task<(string, string)> GetTeacherCreds(int teacherId)
        {
            var teacher = await _repoIdenties.GetIdentityByUserIdAsync(teacherId);

            return (teacher?.Login, teacher?.Password);
        }



        public async Task<GroupHeaderLine> GetTeacherHeaderLineAsync(IdentityType identityType, int teacherId, int groupId)
        {
            var headerLine = new GroupHeaderLine(identityType);

            var dbGroups = await _repoGroups.GetTeacherGroupsAsync(teacherId);

            foreach (int id in dbGroups.OrderBy(g => g))
            {
                headerLine.Selects.Add(new SelectItem
                {
                    ItemId     = id,
                    ItemName   = await _repoGroupTranlations.GetGroupNameByIdAsync(id),
                    IsSelected = id == groupId
                });

                if (id == groupId)
                {
                    headerLine.StudsBtnData = await GetStudentsBtnDataAsyncFrom(groupId);
                }
            }

            return headerLine;

        }
        public async Task<GroupHeaderLine> GetHeaderLineAsync(IdentityType identityType, int compId, int groupId)
        {
            var headerLine = new GroupHeaderLine(identityType);

            var company = await _repoCompany.GetCompanyByIdAsync(compId);

            var dbGroups   = await _repoGroups.GetCompanyGroupsAsync(company.PinnedGroups.ToIdList());

            foreach (var group in dbGroups.OrderBy(g => g.Id))
            {
                headerLine.Selects.Add(new SelectItem
                {
                    ItemId     = group.Id,
                    ItemName   = await _repoGroupTranlations.GetGroupNameByIdAsync(group.Id),
                    IsSelected = group.Id == groupId
                });

                if (group.Id == groupId)
                {
                    headerLine.StudsBtnData = await GetStudentsBtnDataAsyncFrom(groupId);
                }
            }

            return headerLine;
        }
        private async Task<(string, int)> GetStudentsBtnDataAsyncFrom(int groupId)
        {
            return (groupId.ToString(), await GetStudentsCountAsync(new List<int> { groupId }));
        }
        private Task<int> GetStudentsCountAsync(List<int> groups)
        {
            var tcs = new TaskCompletionSource<int>();

            var groupStudents = _repoGroupStudents.GetGroupsStudents(groups);

            tcs.SetResult(groupStudents.Count());

            return tcs.Task;
        }

        public async Task<DtoPaginatorLessons> GetGroupLessonsPaageAsync(int groupId, int page)
        {
            var lessonsPage =  await _repoLessons.GetGroupLessonsPageAsync(groupId, page, _pageSizeLessons);
            if (lessonsPage == null || lessonsPage.Results.Count == 0) return null;

            var paginator = new DtoPaginatorLessons(lessonsPage.RowCount,
                page, _pageSizeLessons, lessonsPage.PageCount);

            foreach (var lesson in lessonsPage.Results)
            {
                paginator.List.Add(new DtoLesson
                {
                    LessonId  = lesson.Id,
                    Teacher   = GetTeacherAsync(lesson.TeacherId),
                    IsActive  = (bool)lesson.Active ? "Дa" : "Нет",
                    StartDate = lesson.Start,
                    EndDate   = lesson.End
                });
            }

            return paginator;
        }
        private async Task<(int, string)> GetTeacherAsync(int teacherId)
        {
            var user = await _repoUsers.GetUserByIdAsync(teacherId);

            return (teacherId, $"{user.FName} {user.SName}");
        }

        public async Task<string> GetGroupLessonsTitleAsync(int lessonId)
        {
            var lessTrans = await _repoLessonsTranlations.GetLessonTranlationsAsync(lessonId);

            return lessTrans.Title;
        }

        public async Task<CorpLesson> CheckAndGetCorpLessonAsync(int groupId, int lessonId, int teacherId)
        {
            var lesson     = await _repoLessons.GetLessonAsync(lessonId);
            var corpLesson = await _repoCorpLessons.GetCorpLessonAsync(lessonId);

            if (corpLesson == null)
            {
                var newCorpLesson = new CorpLesson
                {
                    Id = lessonId,
                    GroupId = groupId,
                    TeacherId = teacherId,
                    StartedAt = lesson.Start,
                    FinishedAt = lesson.End,
                    Note = string.Empty,
                    HasProblem = false
                };

                await _repoCorpLessons.AddNewCorpLessonAsync(newCorpLesson);

                return newCorpLesson;
            }

            return corpLesson;
        }

        public async Task<DtoDoughnutChartView> GetAttendanceAsync(int groupId, DtoDatePicker datePicker)
        {
            var grLessons = await _repoLessons.GetGroupLessonsByIntervalAsync(
                new List<int> { groupId }, datePicker.StartTime, datePicker.EndTime);

            return new DtoDoughnutChartView
            {
                FirstProp = GetAttended(grLessons, out double apsent),
                SeconProp = apsent
            };
        }
        private double GetAttended(List<int> lessonIds, out double apsent)
        {
            int attendCount = 0;
            int appsent = 0;

            if (lessonIds.Count == 0)
            {
                apsent = appsent;
                return attendCount;
            }

            var corpStats = _repoCorpStudentStats.GetCorpstudentStatsAsync(lessonIds).Result;

            foreach (var stat in corpStats)
            {
                if (stat.Attendance) attendCount++;
                else appsent++;
            }

            apsent = corpStats.Count != 0 ? (double)appsent / corpStats.Count * 100 : 0;

            return corpStats.Count != 0 ? (double)attendCount / corpStats.Count * 100 : 0;
        }

        public async Task<DtoDoughnutChartView> GetTestResultsAsync(int groupId, DtoDatePicker datePicker)
        {
            var grStudUsers = await _repoGroups.GetGroupStudentsUserAsync(new List<int> { groupId });
            var testResults = await _repoTestResults.GetCorpTestResultsByIntervalAsync(grStudUsers, datePicker.StartTime, datePicker.EndTime);

            return new DtoDoughnutChartView
            {
                FirstProp = GetResults(testResults, out double secProp),
                SeconProp = secProp
            };
        }
        private double GetResults(List<CorpTestResult> testResults, out double secProp)
        {
            secProp = testResults.Count != 0 ? (double)testResults.Where(t => !t.Completed).Count() / testResults.Count * 100 : 0;

            return testResults.Count != 0 ? (double)testResults.Where(t => t.Completed).Count() / testResults.Count * 100 : 0;
        }

        public async Task<DtoDoughnutTripleView> GetHomeWorksResultAsync(int groupId, DtoDatePicker datePicker)
        {
            var grLessons = await _repoLessons.GetGroupLessonsByIntervalAsync(
                new List<int> { groupId }, datePicker.StartTime, datePicker.EndTime);

            return new DtoDoughnutTripleView
            {
                FirstProp = GetHomeWorks(grLessons, out double no, out double yes),
                SeconProp = no,
                ThirdProp = yes
            };
        }
        private double GetHomeWorks(List<int> lessonIds, out double no, out double yes)
        {
            int done = 0;
            int notDone = 0;
            int partDone = 0;

            if (lessonIds.Count == 0)
            {
                no = yes = done;
            }

            var corpStats = _repoCorpStudentStats.GetCorpstudentStatsAsync(lessonIds).Result;

            foreach (var stat in corpStats)
            {
                if (stat.HomeWork == Yes.ToString("G")) done++;
                if (stat.HomeWork == No.ToString("G")) notDone++;
                if (stat.HomeWork == Partial.ToString("G")) partDone++;
            }

            no  = corpStats.Count != 0 ? (double)notDone / corpStats.Count * 100 : NaN;
            yes = corpStats.Count != 0 ? (double)done / corpStats.Count * 100 : NaN;

            return corpStats.Count != 0 ? (double)partDone / corpStats.Count * 100 : NaN;
        }

        public async Task<DtoDoughnutChartView> GetAverageEfficiencyAsync(int groupId, DtoDatePicker datePicker)
        {
            var grLessons = await _repoLessons.GetGroupLessonsByIntervalAsync(
               new List<int> { groupId }, datePicker.StartTime, datePicker.EndTime);

            var grStudUsers = await _repoGroups.GetGroupStudentsUserAsync(new List<int> { groupId });
            var testResults = await _repoTestResults.GetCorpTestResultsByIntervalAsync(grStudUsers, datePicker.StartTime, datePicker.EndTime);

            var efficiency = GetEfficiency(grLessons, testResults);

            return new DtoDoughnutChartView
            {
                FirstProp = efficiency.Item1,
                SeconProp = efficiency.Item2
            };
        }
        private (double, double) GetEfficiency(List<int> lessonIds, List<CorpTestResult> testResults)
        {
            double attendance = GetAttended(lessonIds, out double apsent);
            double homeWorks = GetHomeWorks(lessonIds, out double no, out double yes);

            double greenEff = CalculateEfficiency
            (
                attend: attendance,
                 tests: GetEfficiencyTestResults(testResults, out double secProp),
                hmWork: homeWorks + yes,
                _appOptions.AttendanceKoef,
                _appOptions.TestsKoef,
                _appOptions.HomeworkKoef
            );

            double redEff = CalculateEfficiency
            (
                attend: apsent,
                 tests: secProp,
                hmWork: no,
                _appOptions.AttendanceKoef,
                _appOptions.TestsKoef,
                _appOptions.HomeworkKoef
            );

            return NormalizePercents(greenEff, redEff);
        }
    }
}
