﻿using Microsoft.Extensions.Options;

using OCSolution.Business.Extensions;
using OCSolution.Business.Models.CompanyPage;
using OCSolution.Business.Models.DTO;
using OCSolution.Business.Models.StudentPage;
using OCSolution.Business.Options;
using OCSolution.Persistance;
using OCSolution.Persistance.Enums;
using OCSolution.Persistance.Models;

using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace OCSolution.Business.Services
{
    public class ServCorpStudents : ServBase, IServCorpStudents
    {
        private readonly IRepoUsers            _repoUsers;
        private readonly IRepoLessons          _repoLessons;
        private readonly IRepoGroups           _repoGroups;
        private readonly IRepoTestResults      _repoTestResults;
        private readonly IRepoGroupStudents    _repoGroupStudents;
        private readonly IRepoCorpStudents     _repoCorpStudents;
        private readonly IRepoCorpStudentStats _repoCorpStudentStats;

        private readonly AppOptions _appOptions;

        private const byte _pageSize = 10;

        public ServCorpStudents(
            IRepoUsers            repoUsers,
            IRepoLessons          repoLessons,
            IRepoGroups           repoGroups,
            IRepoTestResults      repoTestResults,
            IRepoGroupStudents    repoGroupStudents,
            IRepoCorpStudents     repoCorpStudents,
            IRepoCorpStudentStats repoCorpStudentStats,
            IOptions<AppOptions>  appOpt)
        {
            _repoUsers            = repoUsers;
            _repoLessons          = repoLessons;
            _repoGroups           = repoGroups;
            _repoTestResults      = repoTestResults;
            _repoGroupStudents    = repoGroupStudents;
            _repoCorpStudents     = repoCorpStudents;
            _repoCorpStudentStats = repoCorpStudentStats;
            _appOptions           = appOpt.Value;
        }

        public async Task<StudentHeaderLine> GetCompanyStudentPageHeaderlineAsync(IdentityType identityType, string groupIds, int userId)
        {
            var headerLine = new StudentHeaderLine(identityType)
            {
                PrevStateLink = "/dashboard/company"
            };

            var students = _repoGroupStudents.GetGroupsStudents(groupIds.ToIdList());

            foreach (var student in students)
            {
                headerLine.Selects.Add(new SelectItem
                {
                    ItemId     = student.UserId,
                    TargetId   = student.GroupId,
                    ItemName   = await _repoUsers.GetUserFIOAsync(student.UserId),
                    IsSelected = student.UserId == userId
                });
            }

            return headerLine;
        }
        public async Task<StudentHeaderLine> GetGroupStudentPageHeaderlineAsync(IdentityType identityType, int groupId, int userId)
        {
            var headerLine = new StudentHeaderLine(identityType)
            {
                PrevStateLink = "/dashboard/group"
            };

            var students = await _repoGroupStudents.GetGroupStudentsAsync(groupId);

            foreach (var student in students)
            {
                headerLine.Selects.Add(new SelectItem
                {
                    ItemId     = student.UserId,
                    TargetId   = student.GroupId,
                    ItemName   = await _repoUsers.GetUserFIOAsync(student.UserId),
                    IsSelected = student.UserId == userId
                });
            }

            return headerLine;
        }
        public async Task<StudentHeaderLine> GetTeacherStudentPageHeaderlineAsync(IdentityType identityType, int teacherId, int userId)
        {
            var headerLine = new StudentHeaderLine(identityType)
            {
                PrevStateLink = "/dashboard/group"
            };

            var grIds = await _repoGroups.GetTeacherGroupsAsync(teacherId);
            var studs = _repoGroupStudents.GetGroupsStudents(grIds);
            foreach (var student in studs)
            {
                headerLine.Selects.Add(new SelectItem
                {
                    ItemId     = student.UserId,
                    TargetId   = student.GroupId,
                    ItemName   = await _repoUsers.GetUserFIOAsync(student.UserId),
                    IsSelected = student.UserId == userId
                });
            }

            return headerLine;
        }

        public async Task<DtoPaginatorCorpStudents> GetCompanyCorpStudentsPageAsync(List<int> groupsId, int page)
        {
            var students = await _repoGroupStudents.GetPagedGroupsStudents(groupsId, page, _pageSize);

            if (students == null || students.Results.Count == 0) return null;

            var paginator = new DtoPaginatorCorpStudents(
                students.RowCount, page, _pageSize, students.PageCount);

            foreach (var student in students.Results)
            {
                paginator.List.Add(new DtoCompanyCorpStudent
                {
                    Id             = student.Id,
                    UserId         = student.UserId,
                    GroupNumber_id = student.GroupId,
                    //FullName       = await GetStudentFullNameAsync(student.UserId),
                    //CurrentLvl     = student.CurrentLevel,
                    //StartLvl       = student.StartLevel
                    // to do: get other props asinchronus in partial view
                });
            }

            return paginator;
        }
        public async Task<string> GetStudentFullNameAsync(int userId)
        {
            var user = await _repoUsers.GetUserByIdAsync(userId);

            return $"{user?.FName} {user?.SName}";
        }

        public async Task<double> GetStudentAttendanceAsync(int userId)
        {
            var stats = await _repoCorpStudentStats.GetAttendanceStatsByUserIdAsync(userId);
            return stats.Item1 != 0 ? stats.Item2 / stats.Item1 * 100 : 100;
        }

        public async Task<(int, int)> GetStudentAbsentsAsync(int userId)
        {
            var stats = await _repoCorpStudentStats
                .GetAttendanceStatsByUserIdAsync(userId);

            return ((int)stats.Item1, stats.Item3);
        }

        public async Task<double> GetStudentEffectivityAsync(int userId)
        {
            var attStats = await _repoCorpStudentStats.GetAttendanceStatsByUserIdAsync(userId);
            double attendanceAvrg = _appOptions.AttendanceKoef * (attStats.Item1 != 0 ? attStats.Item2 / attStats.Item1 * 100 : 0);

            var hwStats = await _repoCorpStudentStats.GetHomeworkStataByUserIdAsync(userId);
            double hmworkAvrg = _appOptions.HomeworkKoef * (hwStats.Item1 != 0 ? (hwStats.Item2 + hwStats.Item3) / hwStats.Item1 * 100 : 0);

            var tstRes = await _repoTestResults.GetCorpTestResultsByUserId(userId);
            double resAvrg = _appOptions.TestsKoef * GetEfficiencyTestResults(tstRes);

            var result = CalculateEfficiency(attendanceAvrg, resAvrg, hmworkAvrg, 1, 1, 1);
            return result;
        }
        private double GetEfficiencyTestResults(List<CorpTestResult> testResults)
        {
            if (testResults == null || testResults.Count == 0) return 0;

            int count = 0;

            foreach (var test in testResults)
            {
                if (test.Completed && int.TryParse(test.Result, out int result)) count += result;
            }

            return (double)count / (double)testResults.Where(t => t.Completed).Count();
        }

        public Task<int> GetStudentCompletedLessonsAsync(int userId)
            => _repoCorpStudentStats.GetCompletedLessonsAsync(userId);

        public Task<(string, string)> GetStudentsLvlsAsync(int userId)
            => _repoCorpStudents.GetCorpStudentLevelsAsync(userId);

        public Task UpdateCorpStudentCurrentLevel(int userId, string lvl)
            => _repoCorpStudents.UpdateCorpStudentCurrentLvlAsync(userId, lvl);

        public async Task AddStudentTestResultAsync(DtoStudentTestResult dto)
        {
            var testResult = await _repoTestResults.GetTestResultAsync(dto.LessonId, dto.TeacherId, dto.UserId);

            if (testResult == null)
            {
                await _repoTestResults.AddTestResultAsync(new CorpTestResult
                {
                    UserId    = dto.UserId,
                    LessonId  = dto.LessonId,
                    TeacherId = dto.TeacherId,
                    CreatedAt = dto.Date,
                    Completed = dto.IsCompleted,
                    Result    = dto.Result,
                    Type      = dto.TestType
                });
                return;
            }

            testResult.Completed = dto.IsCompleted;
            testResult.CreatedAt = dto.Date;
            testResult.Result    = dto.Result;
            testResult.Type      = dto.TestType;

            await _repoTestResults.UpdateCorpTestResultAsync(testResult);
        }

        public async Task<List<DtoLessonForTestResult>> GetLessonsForTestResultAsync(int userId)
        {
            var groups = await _repoGroupStudents.GetGetGroupsByUserIdAsync(userId);
            var lessns = await _repoLessons.GetLessonsContainsCurrentGroupsAsync(groups);

            return lessns.Select(l => new DtoLessonForTestResult
            {
                LessonId  = l.Id,
                TeacherId = l.TeacherId
            }).ToList();
        }

        public async Task<DtoDoughnutTripleView> GetStudentHomeworkStatsAsync(int userId, DtoDatePicker datePicker)
        {
            var stats = await _repoCorpStudentStats.GetCorpstudentStatsAsync(userId, datePicker.StartTime, datePicker.EndTime);

            if (stats.Count == 0) return new DtoDoughnutTripleView { };

            double a1 = stats.Where(s => s.HomeWork == HomeWorkType.Yes.ToString("G")).Count();
            double a = (double)a1 / stats.Count;

            return new DtoDoughnutTripleView
            {
                FirstProp = (double)stats.Where(s => s.HomeWork == HomeWorkType.Yes.ToString("G")).Count() / stats.Count * 100,
                SeconProp = (double)stats.Where(s => s.HomeWork == HomeWorkType.Partial.ToString("G")).Count() / stats.Count * 100,
                ThirdProp = (double)stats.Where(s => s.HomeWork == HomeWorkType.No.ToString("G")).Count() / stats.Count * 100
            };
        }

        public async Task<DtoBarChart> GetStudentListeningBarChartAsync(int userId, DtoDatePicker datePicker)
        {
            var results = await _repoTestResults.GetTypedCorpTestResultsByIntervalAsync(userId,
                TestType.Listening.ToString("G"), datePicker.StartTime, datePicker.EndTime);

            if (results.Count == 0) return null;

            return new DtoBarChart
            {
                Datas = results.OrderBy(r => r.CreatedAt).Select(r => r != null ? int.Parse(r.Result) : 0).ToArray(),
                Dates = results.OrderBy(r => r.CreatedAt).Select(r => r.CreatedAt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)).ToArray()
            };
        }

        public async Task<DtoBarChart> GetStudentSpeakingBarChartAsync(int userId, DtoDatePicker datePicker)
        {
            var results = await _repoTestResults.GetTypedCorpTestResultsByIntervalAsync(userId,
                TestType.Speaking.ToString("G"), datePicker.StartTime, datePicker.EndTime);

            if (results.Count == 0) return null;

            return new DtoBarChart
            {
                Datas = results.OrderBy(r => r.CreatedAt).Select(r => r != null ? int.Parse(r.Result) : 0).ToArray(),
                Dates = results.OrderBy(r => r.CreatedAt).Select(r => r.CreatedAt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)).ToArray()
            };
        }

        public async Task<DtoBarChart> GetStudentGrammarBarChartAsync(int userId, DtoDatePicker datePicker)
        {
            var results = await _repoTestResults.GetTypedCorpTestResultsByIntervalAsync(userId,
                TestType.Grammar.ToString("G"), datePicker.StartTime, datePicker.EndTime);

            if (results.Count == 0) return null;

            return new DtoBarChart
            {
                Datas = results.OrderBy(r => r.CreatedAt).Select(r => r != null ? int.Parse(r.Result) : 0).ToArray(),
                Dates = results.OrderBy(r => r.CreatedAt).Select(r => r.CreatedAt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)).ToArray()
            };
        }

        public async Task<DtoBarChart> GetStudentReadingBarChartAsync(int userId, DtoDatePicker datePicker)
        {
            var results = await _repoTestResults.GetTypedCorpTestResultsByIntervalAsync(userId,
                TestType.Reading.ToString("G"), datePicker.StartTime, datePicker.EndTime);

            if (results.Count == 0) return null;

            return new DtoBarChart
            {
                Datas = results.OrderBy(r => r.CreatedAt).Select(r => r != null ? int.Parse(r.Result) : 0).ToArray(),
                Dates = results.OrderBy(r => r.CreatedAt).Select(r => r.CreatedAt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)).ToArray()
            };
        }
    }
}
