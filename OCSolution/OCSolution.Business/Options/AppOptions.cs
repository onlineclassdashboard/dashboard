﻿namespace OCSolution.Business.Options
{
    public class AppOptions
    {
        public double AttendanceKoef { get; set; }
        public double HomeworkKoef   { get; set; }
        public double TestsKoef      { get; set; }
    }
}
