﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using OCSolution.Business;
using OCSolution.Business.Services;
using OCSolution.Persistance;
using OCSolution.Persistance.Context;
using OCSolution.Persistance.Repositories;

namespace OCSolution.Root
{
    public static class Injector
    {
        public static void InjectPersistance(
            this IServiceCollection services,
                string connectionString)
        {
            services.AddDbContext<CorpContext>(opt =>
            {
                opt.UseMySQL(connectionString);
                opt.EnableDetailedErrors(true);
            });

            services.AddTransient<IRepoIdenties,           RepoIdenties>();
            services.AddTransient<IRepoCompanies,          RepoCompanies>();
            services.AddTransient<IRepoUsers,              RepoUsers>();
            services.AddTransient<IRepoGroups,             RepoGroups>();
            services.AddTransient<IRepoLessons,            RepoLessons>();
            services.AddTransient<IRepoGroupStudents,      RepoGroupStudents>();
            services.AddTransient<IRepoCorpLessons,        RepoCorpLessons>();
            services.AddTransient<IRepoCorpStudentStats,   RepoCorpStudentStats>();
            services.AddTransient<IRepoCorpStudents,       RepoCorpStudents>();
            services.AddTransient<IRepoTestResults,        RepoTestResults>();
            services.AddTransient<IRepoGroupTranlations,   RepoGroupTranlations>();
            services.AddTransient<IRepoLessonsTranlations, RepoLessonsTranlations>();
        }

        public static void InjectBusiness(
            this IServiceCollection services)
        {
            services.AddTransient<IServCompany,      ServCompany>();
            services.AddTransient<IServGroups,       ServGroups>();
            services.AddTransient<IServCorpStudents, ServCorpStudents>();
            services.AddTransient<IServScheduler,    ServScheduler>();
        }
    }
}
