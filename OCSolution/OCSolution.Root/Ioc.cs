﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using OCSolution.Persistance.Context;

namespace OCSolution.Root
{
    public static class Ioc
    {
        public static void AddDependencies(
            this IServiceCollection services,
                IConfiguration      configuration)
        {
            services.InjectPersistance(configuration
                .GetConnectionString(nameof(CorpContext)));
            services.InjectBusiness();
        }
    }
}
