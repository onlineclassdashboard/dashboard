﻿using OCSolution.Persistance.OldModels;
using OCSolution.Persistance.Value;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace OCSolution.Persistance
{
    public interface IRepoGroupStudents
    {
        Task<PagedResult<GroupStudents>> GetPagedGroupsStudents(List<int> groupsId, int page, int size);
        Task<List<GroupStudents>>        GetGroupStudentsAsync(int groupId);
        List<GroupStudents>              GetGroupsStudents(List<int> groupsIds);
        Task<List<int>>                  GetGetGroupsByUserIdAsync(int userId);
    }
}
