﻿using System;
using System.Collections.Generic;

namespace OCSolution.Persistance.OldModels
{
    public partial class Groups
    {
        public int Id { get; set; }
        public int TeacherId { get; set; }
        public int ProductId { get; set; }
        public string GroupColor { get; set; }
        public bool ForDemo { get; set; }
        public bool? Active { get; set; }
    }
}
