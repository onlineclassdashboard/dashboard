﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OCSolution.Persistance.OldModels
{
    public partial class Lessons
    {
        public int Id { get; set; }
        public int TeacherId { get; set; }
        public int GroupId { get; set; }
        public bool? Active { get; set; }

        [Column("lesson_date_start")]
        public DateTime Start { get; set; }

        [Column("lesson_date_end")]
        public DateTime End { get; set; }
    }
}
