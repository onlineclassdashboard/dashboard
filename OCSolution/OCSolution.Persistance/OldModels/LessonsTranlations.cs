﻿using System;
using System.Collections.Generic;

namespace OCSolution.Persistance.OldModels
{
    public partial class LessonsTranlations
    {
        public int Id { get; set; }
        public int LessonId { get; set; }
        public string Lang { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
