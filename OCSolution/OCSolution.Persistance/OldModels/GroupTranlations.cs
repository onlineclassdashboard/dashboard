﻿namespace OCSolution.Persistance.OldModels
{
    public partial class GroupTranlations
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public string Lang { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Benefits    { get; set; }
    }
}
