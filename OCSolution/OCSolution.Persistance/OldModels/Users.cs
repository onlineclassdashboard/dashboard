﻿namespace OCSolution.Persistance.OldModels
{
    public partial class Users
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Type { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FName { get; set; }
        public string SName { get; set; }
        public string Gender { get; set; }
        public int Discount { get; set; }
        public string BirthDate { get; set; }
        public string Phone { get; set; }
        public string PhoneMobile { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string Address { get; set; }
        public string Avatar { get; set; }
        public string Timezone { get; set; }
        public string Currency { get; set; }
        public string LangDef { get; set; }
        public int? Coins { get; set; }
        public bool LngTestDone { get; set; }
        public string LngTestResult { get; set; }
        public bool? IsFreeLesson { get; set; }
        public string SalesManagerInfo { get; set; }
        public string ComeFrom { get; set; }
        public string SocialFacebookPage { get; set; }
        public string SocialTwitterPage { get; set; }
        public string SocialGooglePage { get; set; }
        public string NewPasswordKey { get; set; }
        public string NewEmail { get; set; }
        public string NewEmailKey { get; set; }
        public bool Active { get; set; }
        public bool IsBan { get; set; }
        public string BanReason { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsSaleManager { get; set; }
        public bool IsDefSaleManager { get; set; }
        public bool IsDefTechSupport { get; set; }
        public string LastIp { get; set; }
        public bool IsCheckPc { get; set; }
    }
}
