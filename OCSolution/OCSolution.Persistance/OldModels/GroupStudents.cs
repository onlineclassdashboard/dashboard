﻿namespace OCSolution.Persistance.OldModels
{
    public partial class GroupStudents
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public int? LessonId { get; set; }
        public int? DiscountId { get; set; }

        public int UserId { get; set; }
        public Users User { get; set; }
    }
}
