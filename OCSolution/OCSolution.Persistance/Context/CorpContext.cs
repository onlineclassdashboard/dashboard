﻿using Microsoft.EntityFrameworkCore;

using OCSolution.Persistance.Models;
using OCSolution.Persistance.OldModels;

namespace OCSolution.Persistance.Context
{
    public partial class CorpContext : DbContext
    {
        public CorpContext()
        {
        }

        public CorpContext(DbContextOptions<CorpContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Company>         Companies        { get; set; }
        public virtual DbSet<CorpIdentity>    Identities       { get; set; }
        public virtual DbSet<CorpStudent>     CorpStudents     { get; set; }
        public virtual DbSet<CorpLesson>      CorpLessons      { get; set; }
        public virtual DbSet<CorpTestResult>  CorpTestResults  { get; set; }
        public virtual DbSet<CorpStudentStat> CorpStudentStats { get; set; }

        public virtual DbSet<Users>              Users              { get; set; }
        public virtual DbSet<Groups>             Groups             { get; set; }
        public virtual DbSet<Lessons>            Lessons            { get; set; }
        public virtual DbSet<GroupStudents>      GroupStudents      { get; set; }
        public virtual DbSet<GroupTranlations>   GroupTranlations   { get; set; }
        public virtual DbSet<LessonsTranlations> LessonsTranlations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Groups>(entity =>
            {
                entity.ToTable("groups");

                entity.HasIndex(e => new { e.TeacherId, e.ProductId })
                    .HasName("teacher_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasColumnName("active")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.ForDemo).HasColumnName("for_demo");

                entity.Property(e => e.GroupColor)
                    .HasColumnName("group_color")
                    .HasMaxLength(40)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.ProductId)
                    .HasColumnName("product_id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.TeacherId)
                    .HasColumnName("teacher_id")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("users");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Avatar)
                    .HasColumnName("avatar")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.BanReason)
                    .HasColumnName("ban_reason")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.BirthDate)
                    .HasColumnName("birth_date")
                    .HasMaxLength(10)
                    .HasDefaultValueSql("'''00.00.0000'''");

                entity.Property(e => e.Coins)
                    .HasColumnName("coins")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.ComeFrom)
                    .HasColumnName("come_from")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Country)
                    .HasColumnName("country")
                    .HasMaxLength(5)
                    .HasDefaultValueSql("'''by'''");

                entity.Property(e => e.Currency)
                    .HasColumnName("currency")
                    .HasMaxLength(5)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Discount)
                    .HasColumnName("discount")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(100);

                entity.Property(e => e.FName)
                    .HasColumnName("f_name")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Gender)
                    .HasColumnName("gender")
                    .HasColumnType("enum('male','female')")
                    .HasDefaultValueSql("'''male'''");

                entity.Property(e => e.IsAdmin).HasColumnName("is_admin");

                entity.Property(e => e.IsBan).HasColumnName("is_ban");

                entity.Property(e => e.IsCheckPc).HasColumnName("is_check_pc");

                entity.Property(e => e.IsDefSaleManager).HasColumnName("is_def_sale_manager");

                entity.Property(e => e.IsDefTechSupport).HasColumnName("is_def_tech_support");

                entity.Property(e => e.IsFreeLesson)
                    .IsRequired()
                    .HasColumnName("is_free_lesson")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.IsSaleManager).HasColumnName("is_sale_manager");

                entity.Property(e => e.LangDef)
                    .HasColumnName("lang_def")
                    .HasMaxLength(5)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.LastIp)
                    .IsRequired()
                    .HasColumnName("last_ip")
                    .HasMaxLength(40);

                entity.Property(e => e.LngTestDone).HasColumnName("lng_test_done");

                entity.Property(e => e.LngTestResult)
                    .HasColumnName("lng_test_result")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.NewEmail)
                    .HasColumnName("new_email")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.NewEmailKey)
                    .HasColumnName("new_email_key")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.NewPasswordKey)
                    .HasColumnName("new_password_key")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasMaxLength(200)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.PhoneMobile)
                    .HasColumnName("phone_mobile")
                    .HasMaxLength(20)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.SName)
                    .HasColumnName("s_name")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.SalesManagerInfo)
                    .HasColumnName("sales_manager_info")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.SocialFacebookPage)
                    .HasColumnName("social_facebook_page")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.SocialGooglePage)
                    .HasColumnName("social_google_page")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.SocialTwitterPage)
                    .HasColumnName("social_twitter_page")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Timezone)
                    .HasColumnName("timezone")
                    .HasMaxLength(100)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasColumnType("enum('demo','teacher','student')");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(50)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Zip)
                    .HasColumnName("zip")
                    .HasMaxLength(40)
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<GroupStudents>(entity =>
            {
                entity.ToTable("group_students");

                entity.HasIndex(e => new { e.UserId, e.GroupId, e.DiscountId })
                    .HasName("user_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.DiscountId)
                    .HasColumnName("discount_id")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.GroupId)
                    .HasColumnName("group_id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.LessonId)
                    .HasColumnName("lesson_id")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.UserId)
                    .HasColumnName("user_id")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<Lessons>(entity =>
            {
                entity.ToTable("lessons");

                entity.HasIndex(e => e.TeacherId)
                    .HasName("teacher_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasColumnName("active")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.GroupId)
                    .HasColumnName("group_id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.TeacherId)
                    .HasColumnName("teacher_id")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<GroupTranlations>(entity =>
            {
                entity.ToTable("group_tranlations");

                entity.HasIndex(e => e.GroupId)
                    .HasName("group_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Benefits)
                    .HasColumnName("benefits")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.GroupId)
                    .HasColumnName("group_id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Lang)
                    .IsRequired()
                    .HasColumnName("lang")
                    .HasMaxLength(5);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<LessonsTranlations>(entity =>
            {
                entity.ToTable("lessons_tranlations");

                entity.HasIndex(e => e.LessonId)
                    .HasName("lesston_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Lang)
                    .IsRequired()
                    .HasColumnName("lang")
                    .HasMaxLength(5);

                entity.Property(e => e.LessonId)
                    .HasColumnName("lesson_id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasMaxLength(255);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
