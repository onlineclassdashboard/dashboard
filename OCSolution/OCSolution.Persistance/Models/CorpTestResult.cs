﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OCSolution.Persistance.Models
{
    [Table("corp_test_results")]
    public class CorpTestResult
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("user_id")]
        public int UserId { get; set; }

        [Column("teacher_id")]
        public int TeacherId { get; set; }

        [Column("lesson_id")]
        public int LessonId { get; set; }

        [Column("type", TypeName = "enum('Speaking','Listening','Reading', 'Grammar')")]
        public string Type { get; set; }

        [Column("result")]
        public string Result { get; set; }

        [Column("completed")]
        public bool Completed { get; set; }

        [Column("created_at")]
        public DateTime CreatedAt { get; set; }
    }
}
