﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OCSolution.Persistance.Models
{
    [Table("companies")]
    public class Company
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("learning_start")]
        public DateTime LearningStart { get; set; }

        [Column("created_at")]
        public DateTime CreatedAt { get; set; }

        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; }

        [Column("pinned_groups")]
        public string PinnedGroups { get; set; }

        [Column("hr_id")]
        public int HrId { get; set; }
    }
}
