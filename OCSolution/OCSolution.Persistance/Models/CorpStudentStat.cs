﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OCSolution.Persistance.Models
{
    [Table("corp_students_stats")]
    public class CorpStudentStat
    {
        [Column("id")]
        public long Id { get; set; }

        [Column("user_id")]
        public int UserId { get; set; }

        [Column("lesson_id")]
        public int LessonId { get; set; }

        [Column("attendance")]
        public bool Attendance { get; set; }

        [Column("home_work", TypeName = "enum('Yes','No','Partial')")]
        public string HomeWork { get; set; }

        [Column("lesson_date")]
        public DateTime LessonDate { get; set; }
    }
}
