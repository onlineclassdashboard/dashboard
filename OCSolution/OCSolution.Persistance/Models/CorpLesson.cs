﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace OCSolution.Persistance.Models
{
    [Table("corp_lessons")]
    public class CorpLesson
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("group_id")]
        public int GroupId { get; set; }

        [Column("teacher_id")]
        public int TeacherId { get; set; }

        [Column("has_problem")]
        public bool HasProblem { get; set; }

        [Column("note")]
        public string Note { get; set; }

        [Column("lesson_date_start")]
        public DateTime StartedAt { get; set; }

        [Column("lesson_date_end")]
        public DateTime FinishedAt { get; set; }

    }
}
