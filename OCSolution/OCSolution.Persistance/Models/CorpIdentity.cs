﻿using OCSolution.Persistance.Enums;

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OCSolution.Persistance.Models
{
    [Table("corp_identities")]
    public class CorpIdentity
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("login")]
        public string Login { get; set; }

        [Column("password")]
        public string Password { get; set; }

        [Column("user_id")]
        public int UserId { get; set; }

        [Column("type", TypeName = "enum('admin','teacher','hr')")]
        public string Type { get; set; }

        [Column("f_name")]
        public string FirstName { get; set; }

        [Column("s_name")]
        public string SecondName { get; set; }

        [Column("created_at")]
        public DateTime CreatedAt { get; set; }

        [Column("updated_at")]
        public DateTime UpdatedAt { get; set; }
    }
}
