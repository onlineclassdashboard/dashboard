﻿using System.ComponentModel.DataAnnotations.Schema;

namespace OCSolution.Persistance.Models
{
    [Table("corp_students")]
    public class CorpStudent
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("start_level")]
        public string StartLevel { get; set; }

        [Column("current_level")]
        public string CurrentLevel { get; set; }

        [Column("group_id")]
        public int GroupId { get; set; }
    }
}
