﻿using Microsoft.EntityFrameworkCore;

using OCSolution.Persistance.OldModels;
using OCSolution.Persistance.Value;

using System;
using System.Collections.Generic;
using System.Linq;

namespace OCSolution.Persistance.Extensions
{
    public static class QuryableExtensions
    {
        public static PagedResult<T> GetPaged<T>(
            this IQueryable<T> query, int page, int pageSize) where T : class
        {
            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize    = pageSize,
                RowCount    = query.Count()
            };

            double pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            int skip = (page - 1) * pageSize;
            result.Results = query.Skip(skip).Take(pageSize).ToList();

            return result;
        }

        public static PagedResult<GroupStudents> GetPagedStudents(
            this DbSet<GroupStudents> students, List<int> groupsId, int page, int pageSize)
        {
            int skip = (page - 1) * pageSize;

            var result = students
                .AsNoTracking().Include(c
                => c.User).Where(u
                    => u.User.Type == "student" &&
                        groupsId.Contains(u.GroupId))
                            .ToList();

            var finalRes = result
                        .Skip(skip)
                        .Take(pageSize)
                        .ToList();

            double pageCount = (double)result.Count / pageSize;

            var pageResult = new PagedResult<GroupStudents>
            {
                CurrentPage = page,
                PageSize    = pageSize,
                PageCount   = (int)Math.Ceiling(pageCount),
                RowCount    = result.Count,
                Results     = finalRes
            };

            return pageResult;
        }
    }
}
