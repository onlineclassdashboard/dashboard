﻿using OCSolution.Persistance.OldModels;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace OCSolution.Persistance
{
    public interface IRepoGroupTranlations
    {
        Task<string> GetGroupNameByIdAsync(int id);
        Task<List<GroupTranlations>> GetAllGroupsAsync();
    }
}
