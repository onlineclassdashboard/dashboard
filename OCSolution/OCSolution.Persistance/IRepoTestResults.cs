﻿using OCSolution.Persistance.Models;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OCSolution.Persistance
{
    public interface IRepoTestResults
    {
        Task<List<CorpTestResult>> GetCorpTestResultsByUserId(int userId);
        Task<CorpTestResult>       CheckAndGetCorpTestResult(int lessonId, int teacherId, int userId);
        Task<List<CorpTestResult>> GetTypedCorpTestResultsByIntervalAsync(int userId, string type, DateTime start, DateTime end);
        Task<List<CorpTestResult>> GetCorpTestResultsByIntervalAsync(List<int> userIds, DateTime start, DateTime end);
        Task<CorpTestResult>       GetTestResultAsync(int lessonId, int teacherId, int userId);
        Task                       UpdateCorpTestResultAsync(int lessonId, int teacherId, int userId, string testType, string testResult);
        Task                       UpdateCorpTestResultAsync(CorpTestResult testResult);
        Task                       AddTestResultAsync(CorpTestResult testResult);
    }
}
