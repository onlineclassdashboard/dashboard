﻿namespace OCSolution.Persistance.Enums
{
    public enum IdentityType
    {
        //[Display(Name = "Admin")]
        Admin,
        //[Display(Name = "Teacher")]
        Teacher,
        //[Display(Name = "HR")]
        HR
    }

    public enum HomeWorkType
    {
        //[Display(Name = "Yes")]
        Yes,
        //[Display(Name = "No")]
        No,
        //[Display(Name = "Partial")]
        Partial
    }

    public enum TestType
    {
        //[Display(Name = "Speaking")]
        Speaking,
        //[Display(Name = "Listening")]
        Listening,
        //[Display(Name = "Reading")]
        Reading,
        //[Display(Name = "Grammar")]
        Grammar
    }
}
