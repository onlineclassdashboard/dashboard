﻿using OCSolution.Persistance.OldModels;
using OCSolution.Persistance.Value;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace OCSolution.Persistance
{
    public interface IRepoGroups
    {
        Task<List<Groups>>  GetCompanyGroupsAsync(List<int> groupsId);
        Task<List<int>>     GetGroupStudentsUserAsync(List<int> groupsId);
        Task<List<int>>     GetCompanyTeachersAsync(List<int> groupsId);
        Task<List<int>>     GetTeacherGroupsAsync(int teacherId);
        Task<List<int>>     GetAllGroupsIdsAsync();

        Task<PagedResult<Groups>> GetPagedGroupsResult(List<int> groupsId, int page, int size);
    }
}
