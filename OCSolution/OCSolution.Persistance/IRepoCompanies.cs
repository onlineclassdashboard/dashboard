﻿using OCSolution.Persistance.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace OCSolution.Persistance
{
    public interface IRepoCompanies
    {
        Task<List<Company>> ToListAsync();
        Task<Company>       GetFirstCompanyAsync();
        Task<Company>       GetCompanyByHrIdAsync(int hrId);
        Task<Company>       GetCompanyByIdAsync(int companyId);
        Task                AddCompanyAsync(Company company);
        Task                UpdateCompanyAsync(Company company);


        //Task AddCompanyAsync(Company company);
    }
}
