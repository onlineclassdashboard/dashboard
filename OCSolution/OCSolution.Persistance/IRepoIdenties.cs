﻿using OCSolution.Persistance.Models;

using System.Threading.Tasks;

namespace OCSolution.Persistance
{
    public interface IRepoIdenties
    {
        Task               SaveChangesAsync();
        Task               UpdateIdentityAsync(CorpIdentity identity);
        Task               AddIdentityAsync(CorpIdentity identity);
        Task<CorpIdentity> GetIdentityByLoginPasswordAsync(string login, string password);
        Task<CorpIdentity> GetIdentityByUserIdAsync(int userId);
        Task<CorpIdentity> GetHrIdentityAsync(int hrId);
    }
}
