﻿using OCSolution.Persistance.Models;

using System.Threading.Tasks;

namespace OCSolution.Persistance
{
    public interface IRepoCorpLessons
    {
        Task             AddNewCorpLessonAsync(CorpLesson lesson);
        Task             UpdateCorpLessonAsync(CorpLesson lesson);
        Task<int>        GetCompletedLessonsAsync(int groupId);
        Task<CorpLesson> GetCorpLessonAsync(int lessonId);
    }
}
