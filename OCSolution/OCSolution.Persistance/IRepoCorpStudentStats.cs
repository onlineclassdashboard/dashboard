﻿using OCSolution.Persistance.Enums;
using OCSolution.Persistance.Models;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OCSolution.Persistance
{
    public interface IRepoCorpStudentStats
    {
        Task               CheckAndAddNewCorpStudentStatAsync(string complexId, DateTime lessonDate);
        Task               ChangeCorpStudentAttendanceAsync(string complexId, bool attendance);
        Task               ChangeCorpStudentHomeworkAsync(string complexId, string homework);
        Task<bool>         GetCorpStudentAttendanceAsync(string complexId);
        Task<HomeWorkType> GetCorpStudentHomeworkTypeAsync(string complexId);

        Task<List<CorpStudentStat>> GetCorpstudentStatsAsync(int userId, DateTime startDate, DateTime endDate);
        Task<List<CorpStudentStat>> GetCorpstudentStatsAsync(List<int> lessonIds);
        Task<(double, double, int)> GetAttendanceStatsByUserIdAsync(int userId);
        Task<(double, int, int)>    GetHomeworkStataByUserIdAsync(int userId);
        Task<List<int>>             GetUserLessonsAsync(int userId);
        Task<int>                   GetCompletedLessonsAsync(int userId);
    }
}
