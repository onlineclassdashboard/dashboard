﻿using OCSolution.Persistance.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace OCSolution.Persistance
{
    public interface IRepoCorpStudents
    {
        Task                    UpdateCorpStudentCurrentLvlAsync(int userId, string newLvl);
        Task                    AddNewCorpStudentAsync(CorpStudent corpStudent);
        Task                    UpdateCorpStudentAsync(CorpStudent In);
        Task<CorpStudent>       GetCorpStudentByIdAsync(int id);
        Task<CorpStudent>       GetCorpStudentAsync(int groupId, int userId);
        Task<List<CorpStudent>> GetCompanyCorpStudentsAsync(List<int> groupsIds);
        Task<string>            GetCorpStudentCurrentLevelAsync(int studentId);
        Task<(string, string)>  GetCorpStudentLevelsAsync(int userId);
    }
}
