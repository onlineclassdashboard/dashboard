﻿using OCSolution.Persistance.OldModels;

using System.Threading.Tasks;

namespace OCSolution.Persistance
{
    public interface IRepoUsers
    {
        Task<Users>            GetUserByIdAsync(int userId);
        Task<string>           GetUserFIOAsync(int userId);
        Task<(string, string)> GetFullNameAsync(int userId);
    }
}
