﻿using OCSolution.Persistance.OldModels;

using System.Threading.Tasks;

namespace OCSolution.Persistance
{
    public interface IRepoLessonsTranlations
    {
        Task<LessonsTranlations> GetLessonTranlationsAsync(int lessonId);
    }
}
