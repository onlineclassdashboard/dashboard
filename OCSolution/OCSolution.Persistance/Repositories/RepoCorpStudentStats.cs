﻿using Microsoft.EntityFrameworkCore;

using OCSolution.Persistance.Context;
using OCSolution.Persistance.Enums;
using OCSolution.Persistance.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCSolution.Persistance.Repositories
{
    public class RepoCorpStudentStats : IRepoCorpStudentStats
    {
        private readonly CorpContext _corpContext;

        public RepoCorpStudentStats(CorpContext corpContext)
        {
            _corpContext = corpContext;
        }

        public Task<List<CorpStudentStat>> GetCorpstudentStatsAsync(int userId, DateTime startDate, DateTime endDate)
        {
            return _corpContext.CorpStudentStats.Where(s
                => s.UserId == userId &&
                   s.LessonDate > startDate &&
                   s.LessonDate <= endDate.AddDays(1))
                    .ToListAsync();
        }

        public async Task ChangeCorpStudentAttendanceAsync(string complexId, bool attendance)
        {
            var stat = await _corpContext.CorpStudentStats
                .FindAsync(long.Parse(complexId));

            stat.Attendance = attendance;

            await _corpContext.SaveChangesAsync();
        }

        public async Task ChangeCorpStudentHomeworkAsync(string complexId, string homework)
        {
            var stat = await _corpContext.CorpStudentStats
                .FindAsync(long.Parse(complexId));

            stat.HomeWork = homework;

            await _corpContext.SaveChangesAsync();
        }

        public Task CheckAndAddNewCorpStudentStatAsync(string complexId, DateTime lessonDate)
        {
            string[] data = complexId.Split(',');
            long id = long.Parse($"{data[0]}{data[1]}");

            var stat = _corpContext.CorpStudentStats.FirstOrDefaultAsync(s => s.Id == id).Result;

            if (stat != null) return Task.CompletedTask;

            return AddCorpStatAsync(id, int.Parse(data[0]), int.Parse(data[1]), lessonDate);
        }
        private async Task AddCorpStatAsync(long id, int lessonId, int userId, DateTime lessonDate)
        {
            await _corpContext.CorpStudentStats.AddAsync(new CorpStudentStat
            {
                Id         = id,
                LessonId   = lessonId,
                UserId     = userId,
                Attendance = true,
                HomeWork   = HomeWorkType.No.ToString("G"),
                LessonDate = lessonDate
            });

            await _corpContext.SaveChangesAsync();
        }

        public async Task<bool> GetCorpStudentAttendanceAsync(string complexId)
        {
            var stat = await _corpContext.CorpStudentStats
                .FindAsync(long.Parse(complexId));

            return stat.Attendance;
        }

        public async Task<HomeWorkType> GetCorpStudentHomeworkTypeAsync(string complexId)
        {
            var stat = await _corpContext.CorpStudentStats.FindAsync(long.Parse(complexId));

            return (HomeWorkType)Enum.Parse(typeof(HomeWorkType), stat.HomeWork, true);
        }

        public Task<List<CorpStudentStat>> GetCorpstudentStatsAsync(List<int> lessonIds)
        {
            var stats = _corpContext.CorpStudentStats.Where(s
                => lessonIds.Contains(s.LessonId));

            return stats.ToListAsync();
        }

        public async Task<(double, double, int)> GetAttendanceStatsByUserIdAsync(int userId)
            => (
                    await _corpContext.CorpStudentStats.Where(s => s.UserId == userId).CountAsync(),
                    await _corpContext.CorpStudentStats.Where(s => s.UserId == userId && s.Attendance).CountAsync(),
                    await _corpContext.CorpStudentStats.Where(s => s.UserId == userId && !s.Attendance).CountAsync()
               );

        public async Task<(double, int, int)> GetHomeworkStataByUserIdAsync(int userId)
            => (
                    await _corpContext.CorpStudentStats.Where(s => s.UserId == userId).CountAsync(),
                    await _corpContext.CorpStudentStats.Where(s => s.UserId == userId && s.HomeWork == HomeWorkType.Yes.ToString("G")).CountAsync(),
                    await _corpContext.CorpStudentStats.Where(s => s.UserId == userId && s.HomeWork == HomeWorkType.Partial.ToString("G")).CountAsync()
               );

        public Task<List<int>> GetUserLessonsAsync(int userId)
        {
            return _corpContext.CorpStudentStats
                .Where(s => s.UserId == userId)
                .Select(s => s.LessonId)
                .ToListAsync();
        }

        public Task<int> GetCompletedLessonsAsync(int userId)
            => _corpContext.CorpStudentStats.Where(l
                 => l.UserId == userId &&
                     l.LessonDate.Date <= DateTime.Now.Date)
                     .CountAsync();
    }
}
