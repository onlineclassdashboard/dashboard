﻿using Microsoft.EntityFrameworkCore;

using OCSolution.Persistance.Context;
using OCSolution.Persistance.Models;

using System;
using System.Linq;
using System.Threading.Tasks;

namespace OCSolution.Persistance.Repositories
{
    public class RepoCorpLessons : IRepoCorpLessons
    {
        private readonly CorpContext _context;

        public RepoCorpLessons(CorpContext context)
        {
            _context = context;
        }

        public async Task AddNewCorpLessonAsync(CorpLesson lesson)
        {
            await _context.CorpLessons.AddAsync(lesson);
            await _context.SaveChangesAsync();
        }

        public Task<CorpLesson> GetCorpLessonAsync(int lessonId)
            => _context.CorpLessons.AsNoTracking().FirstOrDefaultAsync(l
                => l.Id == lessonId);

        public Task UpdateCorpLessonAsync(CorpLesson lesson)
        {
            _context.CorpLessons.Update(lesson);
            return _context.SaveChangesAsync();
        }

        public Task<int> GetCompletedLessonsAsync(int groupId)
            => _context.CorpLessons.Where(l
                 => l.GroupId == groupId &&
                     l.FinishedAt.Date <= DateTime.Now.Date)
                     .CountAsync();
    }
}
