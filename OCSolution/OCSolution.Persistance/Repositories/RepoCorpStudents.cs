﻿using Microsoft.EntityFrameworkCore;

using OCSolution.Persistance.Context;
using OCSolution.Persistance.Models;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCSolution.Persistance.Repositories
{
    public class RepoCorpStudents : IRepoCorpStudents
    {
        private readonly CorpContext _context;

        public RepoCorpStudents(CorpContext context)
        {
            _context = context;
        }

        public async Task UpdateCorpStudentCurrentLvlAsync(int userId, string newLvl)
        {
            var student = await _context.CorpStudents.FindAsync(userId);

            student.CurrentLevel = newLvl;

            await _context.SaveChangesAsync();
        }

        public async Task AddNewCorpStudentAsync(CorpStudent corpStudent)
        {
            await _context.CorpStudents.AddAsync(corpStudent);
            await _context.SaveChangesAsync();
        }

        public Task<CorpStudent> GetCorpStudentByIdAsync(int id)
            => _context.CorpStudents.FirstOrDefaultAsync(s => s.Id == id);

        public Task<CorpStudent> GetCorpStudentAsync(int groupId, int userId)
            => _context.CorpStudents.FirstOrDefaultAsync(s
                => s.Id == userId && s.GroupId == groupId);

        public Task<List<CorpStudent>> GetCompanyCorpStudentsAsync(List<int> groupsIds)
        {
            var corpStudents = _context.CorpStudents.Where(s
                => groupsIds.Contains(s.GroupId));

            return corpStudents.ToListAsync();
        }

        public async Task<string> GetCorpStudentCurrentLevelAsync(int userId)
        {
            var student = await _context.CorpStudents
                .FirstOrDefaultAsync(s => s.Id == userId);

            return (student?.CurrentLevel) ?? "не установлен";
        }

        public async Task<(string, string)> GetCorpStudentLevelsAsync(int userId)
        {
            var student = await _context.CorpStudents
                .FirstOrDefaultAsync(s => s.Id == userId);

            return (student?.StartLevel, student?.CurrentLevel);
        }

        public Task UpdateCorpStudentAsync(CorpStudent In)
        {
            _context.CorpStudents.Update(In);
            return _context.SaveChangesAsync();
        }
    }
}
