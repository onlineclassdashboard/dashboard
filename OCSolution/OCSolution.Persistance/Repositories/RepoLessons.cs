﻿using Microsoft.EntityFrameworkCore;

using OCSolution.Persistance.Context;
using OCSolution.Persistance.Extensions;
using OCSolution.Persistance.OldModels;
using OCSolution.Persistance.Value;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCSolution.Persistance.Repositories
{
    public class RepoLessons : IRepoLessons
    {
        private readonly CorpContext _corpContext;

        public RepoLessons(
            CorpContext corpContext)
        {
            _corpContext = corpContext;
        }

        public Task<Lessons> GetLessonAsync(int lessonId)
            => _corpContext.Lessons.AsNoTracking().FirstOrDefaultAsync(l
                => l.Id == lessonId);

        public Task<Lessons> GetCellLessonAsync(int groupId, DateTime date)
            => _corpContext.Lessons.AsNoTracking().FirstOrDefaultAsync(l
                => l.GroupId == groupId && l.Start.Date == date.Date);

        public Task<PagedResult<Lessons>> GetGroupLessonsPageAsync(int groupId, int page, int size)
        {
            var tcs = new TaskCompletionSource<PagedResult<Lessons>>();

            var lessons = _corpContext.Lessons.Where(l
                => l.GroupId == groupId).OrderByDescending(l
                    => l.End).GetPaged(page, size);

            tcs.SetResult(lessons);

            return tcs.Task;
        }

        public Task<List<int>> GetGroupLessonsByIntervalAsync(List<int> groupIds, DateTime start, DateTime end)
        {
            var lessons = _corpContext.Lessons.Where(l
                => groupIds.Contains(l.GroupId) &&
                    l.Start.Date >= start.Date &&
                     l.End.Date <= end.Date);

            return lessons.Select(l => l.Id).ToListAsync();
        }

        public Task<List<Lessons>> GetLessonsContainsCurrentGroupsAsync(List<int> groupsId)
            => _corpContext.Lessons.Where(l => groupsId.Contains(l.GroupId)).ToListAsync();

        public Task<int> GetCompletedLessonsAsync(int groupId)
            => _corpContext.Lessons.Where(l
                => l.GroupId == groupId &&
                    l.End.Date <= DateTime.Now.Date)
                     .CountAsync();
    }
}
