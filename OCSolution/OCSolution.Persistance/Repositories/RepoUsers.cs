﻿using Microsoft.EntityFrameworkCore;

using OCSolution.Persistance.Context;
using OCSolution.Persistance.OldModels;

using System.Threading.Tasks;

namespace OCSolution.Persistance.Repositories
{
    public class RepoUsers : IRepoUsers
    {
        private readonly CorpContext _context;

        public RepoUsers(CorpContext context)
        {
            _context = context;
        }

        public Task<Users> GetUserByIdAsync(int userId)
            => _context.Users.AsNoTracking().FirstOrDefaultAsync(u
                => u.Id == userId);

        public async Task<(string, string)> GetFullNameAsync(int userId)
        {
            var user = await GetUserByIdAsync(userId);

            return (user?.SName, user?.FName);
        }

        public async Task<string> GetUserFIOAsync(int userId)
        {
            var user = await GetUserByIdAsync(userId);

            return $"{user?.SName} {user?.FName}";
        }
    }
}
