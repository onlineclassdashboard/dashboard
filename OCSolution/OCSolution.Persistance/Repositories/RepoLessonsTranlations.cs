﻿using Microsoft.EntityFrameworkCore;

using OCSolution.Persistance.Context;
using OCSolution.Persistance.OldModels;

using System.Threading.Tasks;

namespace OCSolution.Persistance.Repositories
{
    public class RepoLessonsTranlations : IRepoLessonsTranlations
    {
        private readonly CorpContext _context;

        public RepoLessonsTranlations(
            CorpContext context)
        {
            _context = context;
        }

        public Task<LessonsTranlations> GetLessonTranlationsAsync(int lessonId)
            => _context.LessonsTranlations.FirstOrDefaultAsync(l
                => l.LessonId == lessonId);
    }
}
