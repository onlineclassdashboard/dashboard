﻿using Microsoft.EntityFrameworkCore;

using OCSolution.Persistance.Context;
using OCSolution.Persistance.OldModels;

using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace OCSolution.Persistance.Repositories
{
    public class RepoGroupTranlations : IRepoGroupTranlations
    {
        private readonly CorpContext _corpContext;

        public RepoGroupTranlations(
            CorpContext corpContext)
        {
            _corpContext = corpContext;
        }

        public Task<List<GroupTranlations>> GetAllGroupsAsync()
            => _corpContext.GroupTranlations
                .OrderBy(g => g.Name)
                .ToListAsync();

        public async Task<string> GetGroupNameByIdAsync(int id)
            => (await _corpContext.GroupTranlations.AsNoTracking()
                .FirstOrDefaultAsync(g => g.GroupId == id))?.Name;
    }
}
