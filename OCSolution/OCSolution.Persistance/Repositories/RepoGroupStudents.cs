﻿using Microsoft.EntityFrameworkCore;

using OCSolution.Persistance.Context;
using OCSolution.Persistance.Extensions;
using OCSolution.Persistance.OldModels;
using OCSolution.Persistance.Value;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCSolution.Persistance.Repositories
{
    public class RepoGroupStudents : IRepoGroupStudents
    {
        private readonly CorpContext _corpContext;

        public RepoGroupStudents(CorpContext corpContext)
        {
            _corpContext = corpContext;
        }

        public Task<PagedResult<GroupStudents>> GetPagedGroupsStudents(List<int> groupsId, int page, int size)
        {
            var tcs = new TaskCompletionSource<PagedResult<GroupStudents>>();

            var grStudents = _corpContext.GroupStudents.GetPagedStudents(groupsId, page, size);

            tcs.SetResult(grStudents);

            return tcs.Task;
        }

        public Task<List<GroupStudents>> GetGroupStudentsAsync(int id)
            => _corpContext.GroupStudents.AsNoTracking()
                .Where(g => g.GroupId == id && g.User.Type == "student")
                .ToListAsync();

        public Task<List<int>> GetGetGroupsByUserIdAsync(int userId)
            => _corpContext.GroupStudents
                .Where(gs  => gs.UserId == userId)
                .Select(gs => gs.GroupId)
                .ToListAsync();

        private async Task<bool> IsStudentAsync(int userId)
        {
            bool user = await _corpContext.Users.AnyAsync(u
                => u.Id == userId && u.Type == "student");

            return user;
        }

        public List<GroupStudents> GetGroupsStudents(List<int> groupsId)
        {
            var groupStudents = _corpContext.GroupStudents
                .Where(s => groupsId.Contains(s.GroupId))
                .ToList();

            if (groupStudents == null) return new List<GroupStudents>();

            return groupStudents
                    .Where(g => IsStudentAsync(g.UserId).Result)
                    .GroupBy(g => g.UserId)
                    .Select(g => g.First()).ToList();
        }
    }
}
