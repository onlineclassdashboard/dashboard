﻿using Microsoft.EntityFrameworkCore;

using OCSolution.Persistance.Context;
using OCSolution.Persistance.Models;

using System.Threading.Tasks;

namespace OCSolution.Persistance.Repositories
{
    public class RepoIdenties : IRepoIdenties
    {
        private readonly CorpContext _context;

        public RepoIdenties(CorpContext context)
        {
            _context = context;
        }

        public async Task AddIdentityAsync(CorpIdentity identity)
        {
            await _context.Identities.AddAsync(identity);
            await _context.SaveChangesAsync();
        }

        public Task<CorpIdentity> GetIdentityByLoginPasswordAsync(string login, string password)
            => _context.Identities.AsNoTracking().FirstOrDefaultAsync(i
                => i.Login == login && i.Password == password);

        public Task<CorpIdentity> GetIdentityByUserIdAsync(int userId)
            => _context.Identities.AsTracking().FirstOrDefaultAsync(i
                => i.UserId == userId);

        public Task<CorpIdentity> GetHrIdentityAsync(int hrId)
            => _context.Identities.AsTracking().FirstOrDefaultAsync(i
                => i.Id == hrId);

        public Task UpdateIdentityAsync(CorpIdentity identity)
        {
            _context.Identities.Update(identity);
            return _context.SaveChangesAsync();
        }

        public Task SaveChangesAsync()
            => _context.SaveChangesAsync();

    }
}
