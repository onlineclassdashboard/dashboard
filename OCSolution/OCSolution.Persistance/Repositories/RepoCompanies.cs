﻿using Microsoft.EntityFrameworkCore;

using OCSolution.Persistance.Context;
using OCSolution.Persistance.Models;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCSolution.Persistance.Repositories
{
    public class RepoCompanies : IRepoCompanies
    {
        private readonly CorpContext _contex;

        public RepoCompanies(CorpContext contex)
        {
            _contex = contex;
        }

        public Task<Company> GetCompanyByIdAsync(int companyId)
            => _contex.Companies.AsNoTracking().FirstOrDefaultAsync(c
                => c.Id == companyId);

        public Task<Company> GetCompanyByHrIdAsync(int hrId)
            => _contex.Companies.AsNoTracking().FirstOrDefaultAsync(c
                => c.HrId == hrId);

        public Task<Company> GetFirstCompanyAsync()
            => _contex.Companies.AsNoTracking().OrderBy(c
                => c.Name).FirstOrDefaultAsync();

        public Task<List<Company>> ToListAsync()
            => _contex.Companies.AsNoTracking()
                .ToListAsync();

        public async Task AddCompanyAsync(Company company)
        {
            await _contex.Companies.AddAsync(company);
            await _contex.SaveChangesAsync();
        }

        public Task UpdateCompanyAsync(Company company)
        {
            _contex.Companies.Update(company);
            return _contex.SaveChangesAsync();
        }

    }
}
