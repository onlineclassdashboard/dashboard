﻿using Microsoft.EntityFrameworkCore;

using OCSolution.Persistance.Context;
using OCSolution.Persistance.Extensions;
using OCSolution.Persistance.OldModels;
using OCSolution.Persistance.Value;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCSolution.Persistance.Repositories
{
    public class RepoGroups : IRepoGroups
    {
        private readonly CorpContext _corpContext;

        public RepoGroups(CorpContext corpContext)
        {
            _corpContext = corpContext;
        }

        public Task<List<int>> GetAllGroupsIdsAsync()
            => _corpContext.Groups.AsNoTracking()
                .OrderBy(g => g.Id)
                .Select(g => g.Id)
                .ToListAsync();

        public Task<List<Groups>> GetCompanyGroupsAsync(List<int> groupsId)
        {
            var dbGroups = _corpContext.Groups
                .Where(g => groupsId.Contains(g.Id));

            return dbGroups.ToListAsync();
        }

        public Task<List<int>> GetTeacherGroupsAsync(int teacherId)
            => _corpContext.Groups.AsNoTracking()
                .Where(g => g.TeacherId == teacherId)
                .Select(g => g.Id)
                .ToListAsync();

        public Task<PagedResult<Groups>> GetPagedGroupsResult(List<int> groupsId, int page, int size)
        {
            var tcs = new TaskCompletionSource<PagedResult<Groups>>();

            tcs.SetResult(_corpContext.Groups.Where(g => groupsId.Contains(g.Id)).GetPaged(page, size));

            return tcs.Task;
        }

        public async Task<List<int>> GetGroupStudentsUserAsync(List<int> groupsId)
        {
            var lessons = await _corpContext.GroupStudents.AsNoTracking().Where(g
                => groupsId.Contains(g.GroupId)).ToListAsync();

            return lessons.Select(l => l.UserId).ToList();
        }

        public async Task<List<int>> GetCompanyTeachersAsync(List<int> groupsId)
        {
            var groups = await GetCompanyGroupsAsync(groupsId);

            return groups.Select(g => g.TeacherId).ToList();
        }
    }
}
