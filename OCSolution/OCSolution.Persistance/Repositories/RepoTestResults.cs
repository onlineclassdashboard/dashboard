﻿using Microsoft.EntityFrameworkCore;

using OCSolution.Persistance.Context;
using OCSolution.Persistance.Enums;
using OCSolution.Persistance.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OCSolution.Persistance.Repositories
{
    public class RepoTestResults : IRepoTestResults
    {
        private readonly CorpContext _context;

        public RepoTestResults(CorpContext context)
        {
            _context = context;
        }

        public async Task AddTestResultAsync(CorpTestResult testResult)
        {
            await _context.CorpTestResults.AddAsync(testResult);
            await _context.SaveChangesAsync();
        }

        public async Task<CorpTestResult> CheckAndGetCorpTestResult(int lessonId, int teacherId, int userId)
        {
            try
            {
                var result = await _context.CorpTestResults.FirstOrDefaultAsync(r
                    => r.LessonId   == lessonId  &&
                        r.TeacherId == teacherId &&
                          r.UserId  == userId);

                if (result != null) return result;

                var newRes = new CorpTestResult
                {
                    CreatedAt = DateTime.Now,
                    Completed = false,
                    LessonId  = lessonId,
                    TeacherId = teacherId,
                    Result    = string.Empty,
                    UserId    = userId,
                    Type      = TestType.Listening.ToString("G")
                };

                await _context.AddAsync(newRes);
                await _context.SaveChangesAsync();

                return newRes;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Task<List<CorpTestResult>> GetTypedCorpTestResultsByIntervalAsync(int userId, string type, DateTime start, DateTime end)
        {
            var testResults = _context.CorpTestResults.Where(r
                => r.UserId == userId &&
                   r.Type == type &&
                   r.Completed &&
                   r.CreatedAt.Date >= start.Date &&
                   r.CreatedAt.Date <= end.Date);

            return testResults.ToListAsync();
        }

        public Task<List<CorpTestResult>> GetCorpTestResultsByIntervalAsync(List<int> userIds, DateTime start, DateTime end)
        {
            var testResults = _context.CorpTestResults.Where(g
                => userIds.Contains(g.UserId) &&
                    g.CreatedAt.Date >= start.Date &&
                    g.CreatedAt.Date <= end.Date.AddDays(1));

            return testResults.ToListAsync();
        }

        public Task<List<CorpTestResult>> GetCorpTestResultsByUserId(int userId)
            => _context.CorpTestResults.Where(r => r.UserId == userId && r.Completed).ToListAsync();

        public Task<CorpTestResult> GetTestResultAsync(int lessonId, int teacherId, int userId)
            => _context.CorpTestResults.AsNoTracking().FirstOrDefaultAsync(r
                    => r.LessonId   == lessonId  &&
                        r.TeacherId == teacherId &&
                         r.UserId   == userId);

        public async Task UpdateCorpTestResultAsync(int lessonId, int teacherId, int userId, string testType, string testResult)
        {
            var result = await GetTestResultAsync(lessonId, teacherId, userId);

            result.Completed = true;
            result.Type = testType;
            result.Result = testResult;
            result.CreatedAt = DateTime.Now;

            _context.CorpTestResults.Update(result);

            await _context.SaveChangesAsync();
        }

        public Task UpdateCorpTestResultAsync(CorpTestResult testResult)
        {
            _context.CorpTestResults.Update(testResult);

            return _context.SaveChangesAsync();
        }
    }
}
