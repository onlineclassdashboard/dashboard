﻿using OCSolution.Persistance.OldModels;
using OCSolution.Persistance.Value;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OCSolution.Persistance
{
    public interface IRepoLessons
    {
        Task<int>                  GetCompletedLessonsAsync(int groupId);
        Task<Lessons>              GetLessonAsync(int lessonId);
        Task<Lessons>              GetCellLessonAsync(int groupId, DateTime date);
        Task<List<int>>            GetGroupLessonsByIntervalAsync(List<int> groupIds, DateTime start, DateTime end);
        Task<List<Lessons>>        GetLessonsContainsCurrentGroupsAsync(List<int> groupsId);
        Task<PagedResult<Lessons>> GetGroupLessonsPageAsync(int groupId, int page, int size);
    }
}
