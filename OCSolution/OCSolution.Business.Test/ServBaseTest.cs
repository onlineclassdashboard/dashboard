using System;
using NUnit.Framework;
using OCSolution.Business.Services;

namespace OCSolution.Business.Test
{
    public class Tests
    {
        private ServBase _servBase;
        
        [SetUp]
        public void Setup()
        {
            _servBase = new ServBase();
        }

        [Test]
        public void CalculateEfficiencyTest()
        {
            double attend = 100;
            double tests = 25;
            double hmWork = 25; 
            double attendanceKoef = 1;
            double testsKoef = 1;
            double homeworkKoef = 1;
            var greenResult = _servBase.CalculateEfficiency(attend, tests, hmWork, attendanceKoef, testsKoef, homeworkKoef);
            
            Assert.AreEqual(greenResult, 50.0);
        }
        
        [Test]
        public void CalculateEfficiencyTestTwoParams()
        {
            double attend = 100;
            double tests = 0;
            double hmWork = 20; 
            double attendanceKoef = 1;
            double testsKoef = 1;
            double homeworkKoef = 1;
            var greenResult = _servBase.CalculateEfficiency(attend, tests, hmWork, attendanceKoef, testsKoef, homeworkKoef);
            
            Assert.AreEqual(greenResult, 60.0);
        }
        
        [Test]
        public void CalculateEfficiencyTestOneParam()
        {
            double attend = 100;
            double tests = 0;
            double hmWork = 0; 
            double attendanceKoef = 1;
            double testsKoef = 1;
            double homeworkKoef = 1;
            var greenResult = _servBase.CalculateEfficiency(attend, tests, hmWork, attendanceKoef, testsKoef, homeworkKoef);
            
            Assert.AreEqual(greenResult, 100.0);
        }
        
        [Test]
        public void CalculateEfficiencyTestNan()
        {
            double attend = 100;
            double tests = Double.NaN;
            double hmWork = Double.NaN; 
            double attendanceKoef = 1;
            double testsKoef = 1;
            double homeworkKoef = 1;
            var greenResult = _servBase.CalculateEfficiency(attend, tests, hmWork, attendanceKoef, testsKoef, homeworkKoef);
            
            Assert.AreEqual(greenResult, 100.0);
        }
        
        [Test]
        public void CalculateEfficiencyTestOneNan()
        {
            double attend = 50;
            double tests = Double.NaN;
            double hmWork = 50; 
            double attendanceKoef = 1;
            double testsKoef = 1;
            double homeworkKoef = 1;
            var greenResult = _servBase.CalculateEfficiency(attend, tests, hmWork, attendanceKoef, testsKoef, homeworkKoef);
            
            Assert.AreEqual(greenResult, 50.0);
        }
    }
}