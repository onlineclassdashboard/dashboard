﻿using Microsoft.Extensions.Caching.Memory;

using OCSolution.Business.Utils;

using System;

using static OCSolution.Business.Utils.StringConstant;

namespace OnlineClass.Server.Services
{
    public class AppState : IAppState
    {
        private readonly IMemoryCache _cache;
        private readonly IAppSession  _appSession;

        public bool IsInitialized
            => _cache.Get($"{AUTH_STATE}_{_appSession.AppUser.AppUserId}") != null;

        public int SelectedCompany
            => (int)_cache.Get($"{COMPANY_STATE}_{_appSession.AppUser.AppUserId}");
        public int SelectedGroup
            => (int)_cache.Get($"{GROUP_STATE}_{_appSession.AppUser.AppUserId}");
        public int SelectedStudent
            => (int)_cache.Get($"{STUDENT_STATE}_{_appSession.AppUser.AppUserId}");

        public StudentPageState StudentPageState
            => (StudentPageState)_cache.Get($"{nameof(StudentPageState)}_{_appSession.AppUser.AppUserId}");

        public CardAreaState CompanyAreaState
            => (CardAreaState)_cache.Get($"{COMPANY_AREA_STATE}_{_appSession.AppUser.AppUserId}");
        public CardAreaState GroupAreaState
            => (CardAreaState)_cache.Get($"{GROUP_AREA_STATE}_{_appSession.AppUser.AppUserId}");

        public AppState(
            IMemoryCache  cache,
            IAppSession   appSession)
        {
                 _cache = cache;
            _appSession = appSession;
        }

        public void Authorization()
        {
            string cashKey = $"{AUTH_STATE}_{_appSession.AppUser.AppUserId}";

            _cache.Remove(cashKey);
            _cache.Set(cashKey, AUTH_STATE, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(90)
            });
        }
        public void UnAuthorize()
        {
            string cashKey = $"{AUTH_STATE}_{_appSession.AppUser.AppUserId}";

            _cache.Remove(cashKey);
        }

        public void UpdateSelectedCompany(int companyId)
        {
            string cashKey = $"{COMPANY_STATE}_{_appSession?.AppUser?.AppUserId}";

            _cache.Remove(cashKey);
            _cache.Set(cashKey, companyId, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(90)
            });
        }
        public void UpdateSelectedGroup(int groupId)
        {
            string cashKey = $"{GROUP_STATE}_{_appSession.AppUser.AppUserId}";

            _cache.Remove(cashKey);
            _cache.Set(cashKey, groupId, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(90)
            });
        }
        public void UpdateSelectedStudent(int userId)
        {
            string cashKey = $"{STUDENT_STATE}_{_appSession.AppUser.AppUserId}";

            _cache.Remove(cashKey);
            _cache.Set(cashKey, userId, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(90)
            });
        }

        public void UpdateStudentPageState(StudentPageState pageState)
        {
            string cashKey = $"{nameof(StudentPageState)}_{_appSession.AppUser.AppUserId}";

            _cache.Remove(cashKey);
            _cache.Set(cashKey, pageState, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(90)
            });
        }

        public void UpdateCompanyAreaState(CardAreaState areaState)
        {
            string cashKey = $"{COMPANY_AREA_STATE}_{_appSession?.AppUser?.AppUserId}";

            _cache.Remove(cashKey);
            _cache.Set(cashKey, areaState, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(90)
            });
        }
        public void UpdateGroupAreaState(CardAreaState areaState)
        {
            string cashKey = $"{GROUP_AREA_STATE}_{_appSession.AppUser.AppUserId}";

            _cache.Remove(cashKey);
            _cache.Set(cashKey, areaState, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(90)
            });
        }
    }
}
