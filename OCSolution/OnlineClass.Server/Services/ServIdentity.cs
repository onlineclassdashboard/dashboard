﻿using OCSolution.Business;
using OCSolution.Business.Extensions;
using OCSolution.Business.Models;
using OCSolution.Business.Models.DTO;
using OCSolution.Business.Utils;
using OCSolution.Persistance;
using OCSolution.Persistance.Enums;
using OCSolution.Persistance.Models;

using OnlineClass.Server.Extensions;

using System.Threading.Tasks;

namespace OnlineClass.Server.Services
{
    public class ServIdentity : IServIdentity
    {
        private readonly IAppState     _appState;
        private readonly IAppSession   _appSession;
        private readonly IServCompany  _servCompany;
        private readonly IRepoIdenties _repoIdenties;
        private readonly IRepoGroups   _repoGroups;
        private readonly IRepoGroupStudents _groupStudents;

        public ServIdentity(
            IAppState     appState,
            IAppSession   appSession,
            IServCompany  servCompany,
            IRepoIdenties repoIdenties,
            IRepoGroups   repoGroups,
            IRepoGroupStudents groupStudents)
        {
            _appState     = appState;
            _appSession   = appSession;
            _servCompany  = servCompany;
            _repoIdenties = repoIdenties;
            _repoGroups   = repoGroups;
            _groupStudents = groupStudents;
        }

        public async Task<bool> AuthAsync(DtoFormLoginUser authForm)
        {
            var identity = await _repoIdenties
                .GetIdentityByLoginPasswordAsync(authForm.Login, authForm
                .Password);

            if (identity == null) return false;

            CreateSession(identity);
            _appState.Authorization();

            return identity.Type.ToEnum<IdentityType>() switch
            {
                IdentityType.Admin => await InitializeAdminScopeAsync(),
                IdentityType.HR    => await InitializeHrScopeAsync(identity.Id),
                                 _ => await InitializeTeacherScopeAsync(identity.UserId)
            };
        }

        private void CreateSession(CorpIdentity identity)
        {
            var appUser    = new AppUser
            {
                AppUserId  = identity.Id,
                DbUserId   = identity.UserId,
                FirstName  = identity.FirstName,
                SecondName = identity.SecondName,
                Type       = identity.Type
                    .ToEnum<IdentityType>()
            };

            _appSession.UpdateUserSession(appUser);
        }

        private async Task<bool> InitializeAdminScopeAsync()
        {
            var company = await _servCompany.GetSelectedCompanyAsync(null);

            _appState.UpdateSelectedCompany(company.Id);
            _appState.UpdateCompanyAreaState(CardAreaState.COMPANY_STATS);

            _appState.UpdateSelectedGroup(company.PinnedGroups.ToFirstId());
            _appState.UpdateGroupAreaState(CardAreaState.GROUP_STATS);

            return true;
        }

        private async Task<bool> InitializeHrScopeAsync(int id)
        {
            var company = await _servCompany.GetHrCompanyAsync(id);

            if (company == null) return false;

            _appState.UpdateSelectedCompany(company.Id);
            _appState.UpdateCompanyAreaState(CardAreaState.COMPANY_STATS);

            if (company.PinnedGroups == null)
            {
                _appState.UpdateSelectedGroup(0);
                _appState.UpdateGroupAreaState(CardAreaState.GROUP_STATS);

                return true;
            }

            _appState.UpdateSelectedGroup(company.PinnedGroups.ToFirstId());
            _appState.UpdateGroupAreaState(CardAreaState.GROUP_STATS);

            return true;
        }

        private async Task<bool> InitializeTeacherScopeAsync(int id)
        {
            var dbGroups = await _repoGroups.GetTeacherGroupsAsync(id);

            _appState.UpdateSelectedGroup(dbGroups[0]);
            _appState.UpdateGroupAreaState(CardAreaState.GROUP_SCHEDULER);
            _appState.UpdateStudentPageState(StudentPageState.TEACHER_STUDENT_PAGE);

            return true;
        }
    }
}
