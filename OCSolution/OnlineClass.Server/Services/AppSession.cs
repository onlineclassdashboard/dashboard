﻿using Microsoft.AspNetCore.Http;

using OCSolution.Business.Models;

using OnlineClass.Server.Extensions;

using static OCSolution.Business.Utils.StringConstant;

namespace OnlineClass.Server.Services
{
    public class AppSession : SessionBase, IAppSession
    {
        public AppUser AppUser
           => Session.GetObj<AppUser>(SESSION_USER);

        public AppSession(IHttpContextAccessor accessor)
            : base(accessor) { }

        public void RemoveUserSession()
            => Session.Remove(SESSION_USER);

        public void UpdateUserSession(AppUser user)
        {
            Session.Remove(SESSION_USER);
            Session.SetObj(SESSION_USER, user);
        }
    }
}
