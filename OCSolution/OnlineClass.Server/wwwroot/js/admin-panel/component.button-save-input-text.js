﻿$(document).ready(function () {
    var requestToken = $('input[name="__RequestVerificationToken"]').val();

    $(document).on('click touchstart', '.save-input-text', function () {
        $.post('/adminpanel/saveinputtext', {
            container: $(this).attr('data-container'),
            action: $(this).attr('data-trigger'),
            data: $($(this).attr('data-bind')).val(),
            id: $(this).attr('id'),
            __RequestVerificationToken: requestToken
        });
    });
});