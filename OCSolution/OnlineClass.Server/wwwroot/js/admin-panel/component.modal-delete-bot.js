﻿$(document).ready(function () {
    var requestToken = $('input[name="__RequestVerificationToken"]').val();

    $(document).on('click touchstart', '#_delete_bot', function () {
        $.post('/adminpanel/deletebot', {
            botId: $(this).attr('data-target'),
            __RequestVerificationToken: requestToken
        }, function (res) {
            if(res) location.reload();
        });
    });
});