﻿$(document).ready(function () {
    var requestToken = $('input[name="__RequestVerificationToken"]').val();

    $(document).on('change touchstart', '#_change_bots_combo', function () {
        var id = $(this).val();
        $.post('/adminpanel/botscomboselect', {
            botId: $(this).val(),
            __RequestVerificationToken: requestToken
        }, function (res) {
            $('#_sidebar_nav').empty();
            $('#_sidebar_nav').append(res);
            $('#_delete_bot').attr('data-target', id);
            $('#_current_bot_card').attr('data-target', id);
        });
        setCurrentBotCard(id);
    });

    $(document).on('click touchstart', '#_current_bot_card', function () {
        setCurrentBotCard($(this).attr('data-target'));
    });

    function setCurrentBotCard(id) {
        $.post('/adminpanel/currentcombobot', {
            botId: id,
            __RequestVerificationToken: requestToken
        }, function (res) {
            $('#_main_area_card').empty();
            $('#_main_area_card').append(res);
        });
    }
});