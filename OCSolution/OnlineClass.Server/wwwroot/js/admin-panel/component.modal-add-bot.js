﻿$(document).ready(function () {
    var requestToken = $('input[name="__RequestVerificationToken"]').val();

    $(document).on('click touchstart', '#_bot_token_check', function () {
        //$.get('adminpanel/spinnervawe', function (res) {
        //    $('#_token_append_area').empty();
        //    $('#_token_append_area').append(res);
        //});
        $.post('/adminpanel/bottokeninput', {
            token: $('#_bot_token_input').val(),
            __RequestVerificationToken: requestToken
        }, function (res) {
            $('#_token_append_area').empty();
            $('#_token_append_area').append(res);
            setTimeout(function () {
                location.reload();
            }, 1200)
        });
    });
});