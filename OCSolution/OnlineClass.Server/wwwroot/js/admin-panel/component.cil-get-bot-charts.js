﻿$(document).ready(function () {
    var requestToken = $('input[name="__RequestVerificationToken"]').val();

    $(document).on('click touchstart', '#_current_bot_chart', function () {
        $.post('/adminpanel/botcharts', {
            botId: $(this).attr('data-target'),
            __RequestVerificationToken: requestToken
        }, function (res) {
            $('#_main_area_card').empty();
            $('#_main_area_card').append(res);
        });
    });
});