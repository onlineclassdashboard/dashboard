﻿$(document).ready(function () {
    var requestToken = $('input[name="__RequestVerificationToken"]').val();

    $(document).on('click touchstart', '.setting-items', function () {
        $.post('/adminpanel/itemcontainers', {
            containerId: $(this).attr('data-container'),
            botId: $(this).attr('data-target'),
            __RequestVerificationToken: requestToken
        }, function (res) {
            $('#_main_area_card').empty();
            $('#_main_area_card').append(res);
        });
    });
});