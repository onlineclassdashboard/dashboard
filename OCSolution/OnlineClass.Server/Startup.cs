using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using OCSolution.Business.Options;
using OCSolution.Root;

using OnlineClass.Server.Extensions;
using OnlineClass.Server.Middlewares;
using OnlineClass.Server.Services;

using System;

namespace OnlineClass.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages()
                .ConfigureViewLocationFormats();
            services.AddDependencies(Configuration);

            services.AddMemoryCache();
            services.AddDistributedMemoryCache();
            services.AddHttpContextAccessor();
            services.AddSession(options =>
            {
                options.Cookie.Name = ".OC.Session";
                options.IdleTimeout = TimeSpan.FromDays(24);
            });

            services.AddTransient<IAppState,     AppState>();
            services.AddTransient<IAppSession,   AppSession>();
            services.AddTransient<IServIdentity, ServIdentity>();

            services.Configure<AppOptions>(Configuration
                .GetSection(nameof(AppOptions)));
            services.Configure<UiOptions>(Configuration
               .GetSection(nameof(UiOptions)));

            services.Configure<RouteOptions>(options =>
            {
                options.LowercaseUrls = true;
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders   = ForwardedHeaders
                    .XForwardedFor | ForwardedHeaders
                    .XForwardedProto
            });

            app.UseSession();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            //app.UseMiddleware<AuthMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
    }
}
