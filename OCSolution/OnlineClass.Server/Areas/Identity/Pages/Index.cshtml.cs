using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using OCSolution.Business.Models.DTO;
using OCSolution.Persistance.Enums;

using System.Threading.Tasks;

using static OCSolution.Business.Utils.StringConstant;

namespace OnlineClass.Server.Areas.Identity.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IAppSession _appSession;
        private readonly IServIdentity _servIdentity;

        [BindProperty(SupportsGet = true)]
        public string IdentityState { get; set; }

        public IndexModel(
            IAppSession   appSession,
            IServIdentity servIdentity)
        {
            _appSession   = appSession;
            _servIdentity = servIdentity;
        }

        public IActionResult OnGet()
            => Redirect("/identity/login");

        public IActionResult OnGetLogin()
        {
            IdentityState = LOGIN;

            return Page();
        }

        public async Task<IActionResult> OnPostLogin(DtoFormLoginUser loginUser)
        {
            bool answer = await _servIdentity.AuthAsync(loginUser);

            if (!answer)
            {
                TempData["LoginNotification"] = "������������ �� �������";

                IdentityState = LOGIN;

                return Page();
            }

            return _appSession.AppUser.Type switch
            {
                IdentityType.Admin => RedirectToPage("Company", new { area = "Dashboard" }),
                IdentityType.HR    => RedirectToPage("Company", new { area = "Dashboard" }),
                                 _ => RedirectToPage("Group",   new { area = "Dashboard" })
            };
        }
    }
}
