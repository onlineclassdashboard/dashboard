using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;

using OCSolution.Business;
using OCSolution.Business.Models.DTO;
using OCSolution.Business.Options;

using System;
using System.Globalization;
using System.Threading.Tasks;

using static OCSolution.Business.Utils.StringConstant;

namespace OnlineClass.Server.Areas.Dashboard.Pages
{
    public class StudentModel : PageModel
    {
        private readonly IAppSession       _appSession;
        private readonly IAppState         _appState;
        private readonly IServCorpStudents _servCorpStudents;
        private readonly UiOptions         _ui;

        public StudentModel(
            IAppSession       appSession,
            IAppState         appState,
            IServCorpStudents servCorpStudents,
            IOptions<UiOptions> options)
        {
            _appSession       = appSession;
            _appState         = appState;
            _servCorpStudents = servCorpStudents;
            _ui               = options.Value;
        }

        public IActionResult OnGet()
        {
            if (_appSession.AppUser == null ||
                !_appState.IsInitialized)
                return Redirect(IDENTITY_LOGIN);

            return Page();
        }

        public PartialViewResult OnPostChangeStudentSelectItem(int userId, int targetId)
        {
            _appState.UpdateSelectedStudent(userId);
            _appState.UpdateSelectedGroup(targetId);

            return Partial("_Component_SubHeaderLine_Student");
        }

        public PartialViewResult OnPostUpdateStudentAreacard(string date)
        {
            if (date == null) return Partial("_Partial_Student_Card", date);

            var datePicker = new DtoDatePicker(date);

            //if (datePicker.CurrentInterval >= 15) TempData[STUDENT_DATEPICKER_ALERT] = "������ ��������� ���������� 14 ��.";

            //return Partial("_Partial_Student_Card", datePicker.MinIntervalStr);
            return Partial("_Partial_Student_Card", $"{datePicker.StartTime:dd/MM/yyyy} - {datePicker.EndTime:dd/MM/yyyy}");
        }

        public async Task<JsonResult> OnPostUpdateCurrentLvlvAsync(string lvl)
        {
            await _servCorpStudents.UpdateCorpStudentCurrentLevel(_appState.SelectedStudent, lvl);

            return new JsonResult("Ok");
        }

        public async Task<IActionResult> OnPostAddTestResultAsync(string daterange2,
            string lessonData, string testType, string result)
        {
            string[] data = lessonData.Split(',');

            string dateStr = daterange2.Replace('.', '/');
            var datePicker = new DtoDatePicker(dateStr);

            string checkRes = result ?? "0";

            await _servCorpStudents.AddStudentTestResultAsync(new DtoStudentTestResult
            {
                UserId    = _appState.SelectedStudent,
                LessonId  = int.Parse(data[0]),
                TeacherId = int.Parse(data[1]),
                Date      = datePicker.StartTime,
                TestType  = testType,
                Result    = checkRes == string.Empty ? "0" : checkRes
            });

            TempData[STUDENT_TESTRESULT_ALERT] = _ui.Allerts[3];

            return RedirectToPage("Student", new { area = "Dashboard" });
        }

        // Diagrams AJAX area
        public PartialViewResult OnGetSpinner()
            => Partial("_Spinner");

        public async Task<PartialViewResult> OnPostStudentHomeworkAsync(string date)
        {
            try
            {
                string dateStr = date.Replace('.', '/');
                var datePicker = new DtoDatePicker(dateStr);

                var answ = await _servCorpStudents.GetStudentHomeworkStatsAsync(_appState.SelectedStudent, datePicker);

                if (answ?.FirstProp == 0 && answ?.SeconProp == 0 && answ?.ThirdProp == 0) return Partial("_Chart_Empty", true);

                return Partial("_Chart_Homework_Student", new string[]
                {
                    answ?.FirstProp.ToString("0.00", CultureInfo.InvariantCulture),
                    answ?.SeconProp.ToString("0.00", CultureInfo.InvariantCulture),
                    answ?.ThirdProp.ToString("0.00", CultureInfo.InvariantCulture)
                });
            }
            catch (Exception)
            {
                return Partial("_Chart_Empty", true);
            }
        }
        public async Task<PartialViewResult> OnPostChartListeningAsync(string date)
        {
            try
            {
                string dateStr = date.Replace('.', '/');
                var datePicker = new DtoDatePicker(dateStr);

                var answ = await _servCorpStudents.GetStudentListeningBarChartAsync(_appState.SelectedStudent, datePicker);

                if (answ == null) return Partial("_Chart_Empty", true);

                return Partial("_Chart_Bar_Listening", (answ.Dates, answ.Datas));
            }
            catch (Exception)
            {
                return Partial("_Chart_Empty", true);
            }
        }
        public async Task<PartialViewResult> OnPostChartSpeakingAsync(string date)
        {
            try
            {
                string dateStr = date.Replace('.', '/');
                var datePicker = new DtoDatePicker(dateStr);

                var answ = await _servCorpStudents.GetStudentSpeakingBarChartAsync(_appState.SelectedStudent, datePicker);

                if (answ == null) return Partial("_Chart_Empty", true);

                return Partial("_Chart_Bar_Speaking", (answ.Dates, answ.Datas));
            }
            catch (Exception)
            {
                return Partial("_Chart_Empty", true);
            }
        }
        public async Task<PartialViewResult> OnPostChartGrammarAsync(string date)
        {
            try
            {
                string dateStr = date.Replace('.', '/');
                var datePicker = new DtoDatePicker(dateStr);

                var answ = await _servCorpStudents.GetStudentGrammarBarChartAsync(_appState.SelectedStudent, datePicker);

                if (answ == null) return Partial("_Chart_Empty", true);

                return Partial("_Chart_Bar_Grammar", (answ.Dates, answ.Datas));
            }
            catch (Exception)
            {
                return Partial("_Chart_Empty", true);
            }
        }
        public async Task<PartialViewResult> OnPostChartReadingAsync(string date)
        {
            try
            {
                string dateStr = date.Replace('.', '/');
                var datePicker = new DtoDatePicker(dateStr);

                var answ = await _servCorpStudents.GetStudentReadingBarChartAsync(_appState.SelectedStudent, datePicker);

                if (answ == null) return Partial("_Chart_Empty", true);

                return Partial("_Chart_Bar_Reading", (answ.Dates, answ.Datas));
            }
            catch (Exception)
            {
                return Partial("_Chart_Empty", true);
            }
        }
    }
}
