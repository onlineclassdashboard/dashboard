using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;

using Newtonsoft.Json;

using OCSolution.Business;
using OCSolution.Business.Models.DTO;
using OCSolution.Business.Options;
using OCSolution.Business.Utils;

using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

using static OCSolution.Business.Utils.StringConstant;

namespace OnlineClass.Server.Areas.Dashboard.Pages
{
    public class CompanyModel : PageModel
    {
        private readonly IAppSession  _appSession;
        private readonly IAppState    _appState;
        private readonly IServCompany _servCompany;
        private readonly IServGroups  _servGroups;
        private readonly UiOptions    _ui;

        public CompanyModel(
            IAppSession  appSession,
            IAppState    appState,
            IServCompany servCompany,
            IServGroups  servGroups,
            IOptions<UiOptions> options)
        {
            _appSession  = appSession;
            _appState    = appState;
            _servCompany = servCompany;
            _servGroups  = servGroups;
            _ui          = options.Value;
        }

        public IActionResult OnGet()
        {
            if (_appSession.AppUser == null ||
                !_appState.IsInitialized)
                    return Redirect(IDENTITY_LOGIN);

            return Page();
        }

        public PartialViewResult OnGetCard()
        {
            _appState.UpdateCompanyAreaState(CardAreaState.COMPANY_STATS);

            return Partial("_Partial_Company_Card", null);
        }
        public PartialViewResult OnGetNewCompany()
        {
            _appState.UpdateCompanyAreaState(CardAreaState.COMPANY_ADDNEW);

            return Partial("_Partial_Company_New");
        }
        public PartialViewResult OnGetEditCompany()
        {
            _appState.UpdateCompanyAreaState(CardAreaState.COMPANY_EDIT);

            return Partial("_Partial_Company_Update", _appState.SelectedCompany);
        }
        public PartialViewResult OnGetGroups(string groups)
        {
            _appState.UpdateCompanyAreaState(CardAreaState.COMPANY_GROUPS);

            return Partial("_Partial_Company_GroupsCard", (groups, 1));
        }
        public PartialViewResult OnGetStudents(string groups)
        {
            _appState.UpdateCompanyAreaState(CardAreaState.COMPANY_STUDENTS);

            return Partial("_Partial_Company_StudentsCard", (groups, 1));
        }

        public async Task<IActionResult> OnPostCreateNewAsync(DtoFormCompanyAddNew dtoForm)
        {
            int compId = await _servCompany.CreateNewCompanyAsync(dtoForm);

            _appState.UpdateSelectedCompany(compId);
            _appState.UpdateCompanyAreaState(CardAreaState.COMPANY_EDIT);

            TempData[COMPANY_CRUD_ALERT] = _ui.Allerts[0];

            return RedirectToPage("Company", new { area = "Dashboard" });
        }

        public async Task<IActionResult> OnPostUpdateAsync(DtoFormCompanyUpdate dtoForm)
        {
            await _servCompany.UpdateCompanyAsync(dtoForm);

            TempData[COMPANY_CRUD_ALERT] = _ui.Allerts[1];

            return RedirectToPage("Company", new { area = "Dashboard" });
        }

        // Diagrams AJAX area
        public PartialViewResult OnGetSpinner()
            => Partial("_Spinner");
        public PartialViewResult OnPostChangeCompanySelectItem(int companyId)
        {
            _appState.UpdateSelectedCompany(companyId);

            return Partial("_Component_SubHeaderLine");
        }
        public JsonResult OnPostGotoGroups(int groupId)
        {
            _appState.UpdateSelectedGroup(groupId);

            return new JsonResult("Ok");
        }
        public JsonResult OnPostGotoGroupPage(int groupId)
        {
            _appState.UpdateSelectedGroup(groupId);
            _appState.UpdateGroupAreaState(CardAreaState.GROUP_SCHEDULER);

            return new JsonResult("/dashboard/group");
        }
        public JsonResult OnPostGoToStudentPage(int groupId, int userId)
        {
            _appState.UpdateSelectedGroup(groupId);
            _appState.UpdateSelectedStudent(userId);
            _appState.UpdateStudentPageState(StudentPageState.COMPANY_STUDENT_PAGE);

            return new JsonResult("/dashboard/student");
        }

        public async Task<PartialViewResult> OnPostUpdateAreaCardAsync(int companyId)
        {
            var company = await _servCompany.GetSelectedCompanyAsync(companyId);

            return _appState.CompanyAreaState switch
            {
                CardAreaState.COMPANY_ADDNEW   => Partial("_Partial_Company_New"),
                CardAreaState.COMPANY_EDIT     => Partial("_Partial_Company_Update", companyId),
                CardAreaState.COMPANY_GROUPS   => Partial("_Partial_Company_GroupsCard", (company?.PinnedGroups, 1)),
                CardAreaState.COMPANY_STUDENTS => Partial("_Partial_Company_StudentsCard", (company?.PinnedGroups, 1)),
                                             _ => Partial("_Partial_Company_Card")
            };
        }
        public PartialViewResult OnPostChangeDaterangeAsync(string newDaterange)
        {
            _appState.UpdateCompanyAreaState(CardAreaState.COMPANY_STATS);

            return Partial("_Partial_Company_Card", newDaterange);
        }



        public async Task<PartialViewResult> OnPostAttendanceAsync(int companyId, string date)
        {
            try
            {
                string dateStr = date.Replace('.', '/');

                var answ = await _servCompany.GetAttendanceAsync(companyId,
                            new DtoDatePicker(dateStr));

                if (answ?.FirstProp == 0 && answ?.SeconProp == 0) return Partial("_Chart_Empty", true);

                return Partial("_Chart_Attendance", new string[]
                { 
                    answ?.FirstProp.ToString("0.00", CultureInfo.InvariantCulture),
                    answ?.SeconProp.ToString("0.00", CultureInfo.InvariantCulture)
                });
            }
            catch (System.Exception)
            {
                return Partial("_Chart_Empty", true);
            }
        }
        public async Task<PartialViewResult> OnPostPerfomanceAsync(int companyId, string date)
        {
            try
            {
                string dateStr = date.Replace('.', '/');

                var answ = await _servCompany.GetTestResultsAsync(companyId,
                            new DtoDatePicker(dateStr));

                if (answ?.FirstProp == 0 && answ?.SeconProp == 0) return Partial("_Chart_Empty", true);

                return Partial("_Chart_TestResults", new string[]
                {
                    answ?.FirstProp.ToString("0.00", CultureInfo.InvariantCulture),
                    answ?.SeconProp.ToString("0.00", CultureInfo.InvariantCulture)
                });
            }
            catch (System.Exception)
            {
                return Partial("_Chart_Empty", true);
            }
        }
        public async Task<PartialViewResult> OnPostHomeWorksAsync(int companyId, string date)
        {
            try
            {
                string dateStr = date.Replace('.', '/');

                var answ = await _servCompany.GetHomeWorksResultAsync(companyId,
                        new DtoDatePicker(dateStr));

                if (answ?.FirstProp == 0 && answ?.SeconProp == 0 && answ?.ThirdProp == 0) return Partial("_Chart_Empty", true);

                return Partial("_Chart_Homeworks", new string[]
                {
                    answ?.FirstProp.ToString("0.00", CultureInfo.InvariantCulture),
                    answ?.SeconProp.ToString("0.00", CultureInfo.InvariantCulture),
                    answ?.ThirdProp.ToString("0.00", CultureInfo.InvariantCulture)
                });
            }
            catch (System.Exception)
            {
                return Partial("_Chart_Empty", true);
            }
        }
        public async Task<PartialViewResult> OnPostAverageEfficiencyAsync(int companyId, string date)
        {
            try
            {
                string dateStr = date.Replace('.', '/');

                var answ = await _servCompany.GetAverageEfficiencyAsync(companyId,
                        new DtoDatePicker(dateStr));

                if (answ?.FirstProp == 0 && answ?.SeconProp == 0) return Partial("_Chart_Empty", false);

                return Partial("_Chart_AverageEfficiency", new string[]
                {
                    answ?.FirstProp.ToString("0.00", CultureInfo.InvariantCulture),
                    answ?.SeconProp.ToString("0.00", CultureInfo.InvariantCulture)
                });
            }
            catch (System.Exception)
            {
                return Partial("_Chart_Empty", false);
            }
        }

        public PartialViewResult OnGetPinnedStudents(string groups)
        {
            var data = JsonConvert.DeserializeObject<List<int>>(groups);

            if (data == null || data.Count == 0) return Partial("_Partial_EmptyPinnedTable");

            return Partial("_Partial_Table_PinnedStudents", data);
        }

        public async Task<JsonResult> OnPostUpdateCorpUserAsync(string dataCont, string lvl)
        {
            try
            {
                string[] data = dataCont.Split('_');

                await _servGroups.CheckAndAddNewCorpStudentAsync(
                    int.Parse(data[1]), int.Parse(data[2]), lvl);

                return new JsonResult("CorpUser update Ok");
            }
            catch (System.Exception ex)
            {
                return new JsonResult($"{ex.Message}\n{ex.InnerException}\n{ex.StackTrace}");
            }
        }

        public PartialViewResult OnPostPageBtnClickAsync(string groups, int page)
        {
            return Partial("_Partial_Company_Student_CardBody", (groups, page));
        }

        public PartialViewResult OnPostPageBtnClickGroupsAsync(string groups, int page)
        {
            try
            {
                return Partial("_Partial_Company_Group_CardBody", (groups, page));
            }
            catch (System.Exception)
            {
                return null;
            }
        }
    }
}
