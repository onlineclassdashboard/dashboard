using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;

using OCSolution.Business;
using OCSolution.Business.Models.DTO;
using OCSolution.Business.Options;
using OCSolution.Business.Utils;
using OCSolution.Persistance;

using System.Globalization;
using System.Threading.Tasks;

using static OCSolution.Business.Utils.StringConstant;

namespace OnlineClass.Server.Areas.Dashboard.Pages
{
    public class GroupModel : PageModel
    {
        private readonly IAppSession           _appSession;
        private readonly IAppState             _appState;
        private readonly IServGroups           _servGroups;
        private readonly IServScheduler        _servScheduler;
        private readonly IRepoCorpLessons      _repoCorpLessons;
        private readonly IRepoTestResults      _repoTestResults;
        private readonly IRepoCorpStudentStats _corpStudentStats;
        private readonly UiOptions             _ui;

        public GroupModel(
            IAppSession           appSession,
            IAppState             appState,
            IServGroups           servGroups,
            IServScheduler        servScheduler,
            IRepoCorpLessons      repoCorpLessons,
            IRepoTestResults      repoTestResults,
            IRepoCorpStudentStats corpStudentStats,
            IOptions<UiOptions>   options)
        {
            _appSession       = appSession;
            _appState         = appState;
            _servGroups       = servGroups;
            _servScheduler    = servScheduler;
            _repoCorpLessons  = repoCorpLessons;
            _repoTestResults  = repoTestResults;
            _corpStudentStats = corpStudentStats;
            _ui               = options.Value;
        }

        public IActionResult OnGet()
        {
            if (_appSession.AppUser == null ||
                !_appState.IsInitialized)
                return Redirect(IDENTITY_LOGIN);

            return Page();
        }

        public PartialViewResult OnGetCard()
        {
            _appState.UpdateGroupAreaState(CardAreaState.GROUP_STATS);

            return Partial("_Partial_Group_Card", null);
        }
        public PartialViewResult OnGetLessons()
        {
            _appState.UpdateGroupAreaState(CardAreaState.GROUP_LESSONS);

            return Partial("_Partial_Lessons_Card", 1);
        }
        public PartialViewResult OnGetClassCalendar()
        {
            _appState.UpdateGroupAreaState(CardAreaState.GROUP_SCHEDULER);

            return Partial("_Partial_ClassCalendar_Card");
        }
        public PartialViewResult OnGetCustomClassCalendar(int month, int year)
        {
            var scheduler = _servScheduler.GetCustomScheduler(_appState.SelectedGroup, year, month);

            return Partial("_Partial_Calendar", scheduler);
        }
        public PartialViewResult OnGetStudents(string group)
        {
            _appState.UpdateGroupAreaState(CardAreaState.GROUP_STUDENTS);

            return Partial("_Partial_GroupStudents_Card", (group, 1));
        }

        public async Task<PartialViewResult> OnPostAttendanceAsync(string date)
        {
            try
            {
                string dateStr = date.Replace('.', '/');

                var answ = await _servGroups.GetAttendanceAsync(_appState.SelectedGroup,
                            new DtoDatePicker(dateStr));

                if (answ?.FirstProp == 0 && answ?.SeconProp == 0) return Partial("_Chart_Empty", true);

                return Partial("_Chart_Attendance_Groups", new string[]
                {
                    answ?.FirstProp.ToString("0.00", CultureInfo.InvariantCulture),
                    answ?.SeconProp.ToString("0.00", CultureInfo.InvariantCulture)
                });

            }
            catch (System.Exception)
            {
                return Partial("_Chart_Empty", true);
            }
        }
        public async Task<PartialViewResult> OnPostPerfomanceAsync(string date)
        {
            try
            {
                string dateStr = date.Replace('.', '/');

                var answ = await _servGroups.GetTestResultsAsync(_appState.SelectedGroup,
                            new DtoDatePicker(dateStr));

                if (answ?.FirstProp == 0 && answ?.SeconProp == 0) return Partial("_Chart_Empty", true);

                return Partial("_Chart_TestResults_Groups", new string[]
                {
                    answ?.FirstProp.ToString("0.00", CultureInfo.InvariantCulture),
                    answ?.SeconProp.ToString("0.00", CultureInfo.InvariantCulture)
                });
            }
            catch (System.Exception)
            {
                return Partial("_Chart_Empty", true);
            }
        }
        public async Task<PartialViewResult> OnPostHomeWorksAsync(string date)
        {
            try
            {
                string dateStr = date.Replace('.', '/');

                var answ = await _servGroups.GetHomeWorksResultAsync(_appState.SelectedGroup,
                            new DtoDatePicker(dateStr));

                if (answ?.FirstProp == 0 && answ?.SeconProp == 0 && answ?.ThirdProp == 0) return Partial("_Chart_Empty", true);

                return Partial("_Chart_Homeworks_Groups", new string[]
                {
                    answ?.FirstProp.ToString("0.00", CultureInfo.InvariantCulture),
                    answ?.SeconProp.ToString("0.00", CultureInfo.InvariantCulture),
                    answ?.ThirdProp.ToString("0.00", CultureInfo.InvariantCulture)
                });
            }
            catch (System.Exception)
            {
                return Partial("_Chart_Empty", true);
            }
        }
        public async Task<PartialViewResult> OnPostAverageEfficiencyAsync(string date)
        {
            try
            {
                string dateStr = date.Replace('.', '/');

                var answ = await _servGroups.GetAverageEfficiencyAsync(_appState.SelectedGroup,
                            new DtoDatePicker(dateStr));

                if (answ?.FirstProp == 0 && answ?.SeconProp == 0) return Partial("_Chart_Empty", false);

                return Partial("_Chart_AverageEfficiency_Groups", new string[]
                {
                    answ?.FirstProp.ToString("0.00", CultureInfo.InvariantCulture),
                    answ?.SeconProp.ToString("0.00", CultureInfo.InvariantCulture)
                });
            }
            catch (System.Exception)
            {
                return Partial("_Chart_Empty", false);
            }
        }

        public JsonResult OnPostGoToStudentPage(int userId)
        {
            _appState.UpdateSelectedStudent(userId);
            _appState.UpdateStudentPageState(StudentPageState.GROUP_STUDENT_PAGE);

            return new JsonResult("/dashboard/student");
        }


        // AJAX handlers
        public PartialViewResult OnGetSpinner()
            => Partial("_Spinner");
        public PartialViewResult OnPostChangeGroupSelectItem(int groupId)
        {
            _appState.UpdateSelectedGroup(groupId);

            return Partial("_Component_SubHeaderLine_Group");
        }

        public PartialViewResult OnPostUpdateAreaCard(int groupId)
        {
            return _appState.GroupAreaState switch
            {
                CardAreaState.GROUP_LESSONS   => Partial("_Partial_Lessons_Card", 1),
                CardAreaState.GROUP_SCHEDULER => Partial("_Partial_ClassCalendar_Card"),
                CardAreaState.GROUP_STATS     => Partial("_Partial_Group_Card"),
                CardAreaState.GROUP_STUDENTS  => Partial("_Partial_GroupStudents_Card", (groupId.ToString(), 1)),
                                            _ => Partial("_Partial_Group_Card")
            };
        }
        public PartialViewResult OnPostChangeDaterangeAsync(string newDaterange)
        {
            _appState.UpdateCompanyAreaState(CardAreaState.GROUP_STATS);

            return Partial("_Partial_Group_Card", newDaterange);
        }

        public PartialViewResult OnPostPageBtnClick(string group, int page)
        {
            return Partial("_Partial_GroupStudents_CardBody", (group, page));
        }

        public PartialViewResult OnPostLessonsPageBtnClick(int page)
        {
            return Partial("_Partial_Lessons_CardBody", (_appState.SelectedGroup, page));
        }

        public PartialViewResult OnGetLessonView(string btnData)
        {
            string[] data = btnData.Split(',');

            return Partial("_Partial_lesson_ModalBody", (int.Parse(data[0]), int.Parse(data[1]), int.Parse(data[2])));
        }

        public async Task<JsonResult> OnPostChangeStudentAttendanceAsync(string dataId, bool attendance)
        {
            await _corpStudentStats.ChangeCorpStudentAttendanceAsync(dataId, attendance);

            return new JsonResult("Ok");
        }
        public async Task<JsonResult> OnPostChangeStudentHomeworkAsync(string dataId, string homeWork)
        {
            await _corpStudentStats.ChangeCorpStudentHomeworkAsync(dataId, homeWork);

            return new JsonResult(homeWork);
        }


        public async Task<JsonResult> OnPostUpdateCorpLessonAsync(int id, bool hasProblem, string note)
        {
            var lesson = await _repoCorpLessons.GetCorpLessonAsync(id);

            lesson.Note       = note;
            lesson.HasProblem = hasProblem;

            await _repoCorpLessons.UpdateCorpLessonAsync(lesson);

            TempData[CORPLESSON_UPDATE_ALERT] = _ui.Allerts[2];

            return new JsonResult("Ok");
        }
        public async Task<JsonResult> OnPostUpdateTestResultAsync(string btnContent, string testType, string testResult)
        {
            string[] btnData = btnContent.Split(',');

            await _repoTestResults.UpdateCorpTestResultAsync(int.Parse(btnData[0]), int.Parse(btnData[1]), int.Parse(btnData[2]), testType, testResult);

            return new JsonResult("Ok");
        }
    }
}
