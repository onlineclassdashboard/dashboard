using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using OCSolution.Persistance.Enums;

using static OCSolution.Business.Utils.StringConstant;

namespace OnlineClass.Server.Areas.Dashboard.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IAppSession _appSession;
        private readonly IAppState   _appState;

        public IndexModel(
            IAppSession appSession,
            IAppState   appState)
        {
            _appSession = appSession;
            _appState   = appState;
        }

        public IActionResult OnGet()
        {
            if (_appSession.AppUser == null ||
                !_appState.IsInitialized)
                return Redirect(IDENTITY_LOGIN);

            if (_appSession.AppUser.Type == IdentityType.Teacher)
                 return Redirect("/dashboard/group");
            else return Redirect("/dashboard/company");
        }

        public JsonResult OnPostLogout()
        {
            _appState.UnAuthorize();
            _appSession.RemoveUserSession();

            return new JsonResult("Ok");
        }
    }
}
