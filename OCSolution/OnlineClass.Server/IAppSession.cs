﻿using OCSolution.Business.Models;

namespace OnlineClass.Server
{
    public interface IAppSession
    {
        AppUser AppUser { get; }

        void RemoveUserSession();
        void UpdateUserSession(AppUser user);
    }
}
