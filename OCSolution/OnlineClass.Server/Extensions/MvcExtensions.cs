﻿using Microsoft.Extensions.DependencyInjection;

namespace OnlineClass.Server.Extensions
{
    public static class MvcExtensions
    {
        public static void ConfigureViewLocationFormats(this IMvcBuilder mvcBuilder)
        {
            mvcBuilder.AddRazorOptions(options =>
            {
                options.AreaPageViewLocationFormats.Add("/Areas/Dashboard/Views/{0}.cshtml");
                options.AreaPageViewLocationFormats.Add("/Areas/Dashboard/UiElements/{0}.cshtml");
                options.AreaPageViewLocationFormats.Add("/Areas/Dashboard/Components/Sidebar/{0}.cshtml");
                options.AreaPageViewLocationFormats.Add("/Areas/Dashboard/Components/HeaderlineMain/{0}.cshtml");
                options.AreaPageViewLocationFormats.Add("/Areas/Dashboard/Components/MainArea/{0}.cshtml");
                options.AreaPageViewLocationFormats.Add("/Areas/Dashboard/Components/Modals/{0}.cshtml");
                options.AreaPageViewLocationFormats.Add("/Areas/Dashboard/Charts/{0}.cshtml");
            });
        }
    }
}
