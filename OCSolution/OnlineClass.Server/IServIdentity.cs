﻿using OCSolution.Business.Models.DTO;

using System.Threading.Tasks;

namespace OnlineClass.Server
{
    public interface IServIdentity
    {
        Task<bool> AuthAsync(DtoFormLoginUser authForm);
    }
}
