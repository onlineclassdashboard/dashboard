﻿using Microsoft.AspNetCore.Http;

using System.Threading.Tasks;

using static OCSolution.Business.Utils.StringConstant;

namespace OnlineClass.Server.Middlewares
{
    public class AuthMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IAppState       _appState;
        private readonly IAppSession     _appSession;

        public AuthMiddleware(
            RequestDelegate next,
            IAppState       appState,
            IAppSession     appSession)
        {
                  _next = next;
              _appState = appState;
            _appSession = appSession;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (context.Request.Path == IDENTITY_LOGIN) await _next.Invoke(context);

            if (_appSession.AppUser == null ||
                !_appState.IsInitialized)
                context.Response.Redirect(IDENTITY_LOGIN);
            else await _next.Invoke(context);
        }
    }
}
