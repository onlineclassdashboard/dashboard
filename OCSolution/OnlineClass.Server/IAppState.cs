﻿using OCSolution.Business.Utils;

namespace OnlineClass.Server
{
    public interface IAppState
    {
        bool IsInitialized   { get; }
        int  SelectedCompany { get; }
        int  SelectedGroup   { get; }
        int  SelectedStudent { get; }

        StudentPageState StudentPageState { get; }

        CardAreaState CompanyAreaState { get; }
        CardAreaState GroupAreaState   { get; }

        void Authorization();
        void UnAuthorize();

        void UpdateSelectedCompany(int companyId);
        void UpdateSelectedGroup(int groupIdId);
        void UpdateSelectedStudent(int userId);

        void UpdateStudentPageState(StudentPageState pageState);

        void UpdateCompanyAreaState(CardAreaState areaState);
        void UpdateGroupAreaState(CardAreaState areaState);
    }
}
